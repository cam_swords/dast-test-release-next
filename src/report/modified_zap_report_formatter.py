import itertools
from collections import OrderedDict

from src.models import Alert, Alerts
from .url import URL


class ModifiedZapReportFormatter:

    def __init__(self, system):
        self.system = system

    def format_report(self, version, alerts, scanned_resources, end_date_time, spider_scan, spider_scan_results):
        formatted = OrderedDict()

        if spider_scan is None or len(spider_scan) == 0:
            raise RuntimeError('Unable to create DAST report as there is no spider scan')

        if spider_scan_results is None or len(spider_scan_results) == 0:
            raise RuntimeError('Unable to create DAST report as there are no spider scan results')

        # Legacy DAST format
        formatted['@generated'] = self.system.current_date_time().strftime('%a, %d %b %Y %H:%M:%S')
        formatted['@version'] = version
        formatted['site'] = self._format_sites(alerts)
        formatted['spider'] = spider = OrderedDict()
        spider['progress'] = spider_scan.get('progress', '')
        spider['result'] = OrderedDict()
        spider['result']['urlsInScope'] = self._format_urls(self._collect(spider_scan_results, 'urlsInScope'))
        spider['result']['urlsIoError'] = self._format_urls(self._collect(spider_scan_results, 'urlsIoError'))
        spider['result']['urlsOutOfScope'] = sorted(self._collect(spider_scan_results, 'urlsOutOfScope'))
        spider['state'] = spider_scan.get('state', '')

        return formatted

    def sort_by(self, *properties):
        return lambda value: ':'.join([value.get(prop, '') for prop in properties])

    def _collect(self, values, results_property):
        properties = [value.get(results_property, []) for value in values]
        return list(itertools.chain(*properties))

    def _format_sites(self, alerts):
        formatted = []

        sorted_alerts = sorted(alerts, key=lambda a: self._group_sites_by(a))

        for _, host_alerts in itertools.groupby(sorted_alerts, lambda a: self._group_sites_by(a)):
            formatted.append(self._format_site(list(host_alerts)))

        return formatted

    def _group_sites_by(self, alert: Alert):
        url = URL.parse(alert.url)
        return url.scheme_and_host

    def _format_site(self, alerts):
        if len(alerts) == 0:
            raise RuntimeError('Attempting to format site when there are no alerts')

        url = URL.parse(alerts[0].url)

        formatted = OrderedDict()
        formatted['@host'] = url.host
        formatted['@name'] = url.scheme_and_host
        formatted['@port'] = str(url.port)
        formatted['@ssl'] = 'true' if url.is_secure else 'false'
        formatted['alerts'] = self._format_alerts(alerts)
        return formatted

    def _format_alerts(self, alerts):
        formatted = []
        sorted_alerts = sorted(alerts, key=lambda a: self._group_alerts_by(a))

        for _, plugin_alerts in itertools.groupby(sorted_alerts, lambda a: self._group_alerts_by(a)):
            formatted.append(self._format_alert(Alerts(list(plugin_alerts))))

        return sorted(formatted, key=lambda a: (-int(a.get('riskcode', '1')), a.get('name', '')))

    def _group_alerts_by(self, alert):
        return f'{alert.rule_id}::{alert.risk}::{alert.confidence}'

    def _format_alert(self, alerts):
        if len(alerts) == 0:
            raise RuntimeError('Unable to format alerts when there are none present')

        first_alert = alerts[0]

        formatted = OrderedDict()
        formatted['alert'] = first_alert.name
        formatted['confidence'] = self._format_confidence(first_alert.confidence)
        formatted['count'] = str(len(alerts))
        formatted['cweid'] = first_alert.cwe_id
        formatted['desc'] = self._format_lines_as_html(first_alert.description)
        formatted['instances'] = self._format_alert_instances(alerts)
        formatted['name'] = first_alert.name
        formatted['otherinfo'] = self._format_lines_as_html(first_alert.detail)
        formatted['pluginid'] = first_alert.rule_id
        formatted['reference'] = self._format_lines_as_html(first_alert.reference, empty_value='<p></p>')
        formatted['riskcode'] = first_alert.riskcode
        formatted['riskdesc'] = f'{first_alert.risk} ({first_alert.confidence})'
        formatted['solution'] = self._format_lines_as_html(first_alert.solution, empty_value='<p></p>')
        formatted['sourceid'] = first_alert.source_id
        formatted['wascid'] = first_alert.wasc_id

        if not formatted['pluginid'].valid():
            raise RuntimeError('Unable to create DAST report as '
                               "there is no pluginid for alert '{}'".format(first_alert.id))

        return formatted

    def _format_non_zero_text_digit(self, value):
        if not value or not value.isdigit() or value == '0':
            return ''

        return value

    def _format_lines_as_html(self, content, empty_value=''):
        if not content:
            return empty_value

        paragraphs = [f'<p>{line}</p>' for line in content.replace('\r', '').split('\n')]
        return ''.join(paragraphs)

    def _format_confidence(self, zap_confidence):
        return {'False Positive': '0',
                'Low': '1',
                'Medium': '2',
                'High': '3',
                'Confirmed': '4'}.get(zap_confidence, '1')

    def _format_alert_instances(self, alerts):
        sorted_alerts = sorted(alerts, key=alerts.sort_by(['url', 'method', 'param', 'attack']))
        return [self._format_alert_instance(alert) for alert in sorted_alerts]

    def _format_alert_instance(self, alert):
        formatted = OrderedDict()
        formatted['attack'] = alert.attack
        formatted['evidence'] = alert.evidence
        formatted['method'] = alert.method
        formatted['param'] = alert.param
        formatted['uri'] = alert.url
        return formatted

    def _format_urls(self, urls):
        sorted_urls = sorted(urls, key=self.sort_by('url', 'method'))
        return [self._format_url(url) for url in sorted_urls]

    def _format_url(self, url):
        formatted = OrderedDict()
        formatted['method'] = url.get('method', '')
        formatted['processed'] = url.get('processed', '')
        formatted['reasonNotProcessed'] = url.get('reasonNotProcessed', '')
        formatted['statusCode'] = url.get('statusCode', '')
        formatted['statusReason'] = url.get('statusReason', '')
        formatted['url'] = url.get('url', '')

        return formatted
