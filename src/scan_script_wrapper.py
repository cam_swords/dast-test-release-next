import logging

from src.custom_hooks import CustomHooks
from src.services.active_scan_policy import ActiveScanPolicy
from src.system import System
from src.zap_gateway import ZAPServer, ZAProxy


class ScanScriptWrapper():

    def __init__(self,
                 config,
                 zap_log_configuration,
                 active_scan_policy: ActiveScanPolicy,
                 zap_server: ZAPServer,
                 zaproxy: ZAProxy,
                 custom_hooks: CustomHooks):
        self.config = config
        self.system = System()
        self.zap_log_configuration = zap_log_configuration
        self.active_scan_policy = active_scan_policy
        self.zap_server = zap_server
        self._zaproxy = zaproxy
        self._custom_hooks = custom_hooks

    def run(self):
        logging.info(f'Running DAST {self.system.dast_version()} on Python {self.system.python_version()}')

        logging.debug('writing zap log configuration')
        self._write_zap_log_configuration()

        if self.config.full_scan and self.config.exclude_rules:
            logging.debug('writing zap active scan policy')
            with open(self.active_scan_policy.get_path(), 'w') as handle:
                self.active_scan_policy.write_policy(handle)

        # Hide "Starting new HTTP connection" messages
        logging.getLogger('requests').setLevel(logging.DEBUG)

        zap_daemon = self.zap_server.start()
        zap_client = self._zaproxy.connect(zap_daemon.proxy_endpoint())

        self._custom_hooks.handover_to_dast(zap_client, zap_daemon)

    def _write_zap_log_configuration(self):
        with open(self.zap_log_configuration.get_path(), 'w+') as handle:
            self.zap_log_configuration.write_log_properties(handle)
