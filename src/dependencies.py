import os
import sys
import uuid

from src.config import ConfigurationParser
from src.container import Container
from src.custom_hooks import CustomHooks
from src.models import WebApplicationSecurityConsortium
from src.models.active_scan.factory import APIScanPolicyFactory, DefaultScanPolicyFactory
from src.report import CombinedReportFormatter, ModifiedZapReportFormatter, SecurityReportFormatter
from src.report.security_report_formatters import ScanReportFormatter, VulnerabilityReportFormatter
from src.scan_script_wrapper import ScanScriptWrapper
from src.services.active_scan_policy import ActiveScanPolicy
from src.services.script_finder import ScriptFinder
from src.system import System
from src.zap_gateway import AlertsParser, ExcludeRulesConfigurationBuilder, HttpHeadersParser, \
    RequestHeadersConfigurationBuilder, UserConfigurationBuilder, ZAPServer, ZAProxy
from src.zap_gateway.zap_database import HttpStatusParser, MessageResultsParser, ZAPDatabase
from src.zap_log_configuration import ZAPLogConfiguration


def initialize_container() -> Container:
    if Container.CACHE:
        return Container.CACHE

    c = Container()
    c.system = System()
    c.start_date_time = c.system.current_date_time()
    c.config = ConfigurationParser().parse(sys.argv[1:], os.environ)

    c.exclude_rules_builder = ExcludeRulesConfigurationBuilder(c.config)
    c.request_headers_builder = RequestHeadersConfigurationBuilder(c.config)
    c.user_configuration_builder = UserConfigurationBuilder(c.config)
    c.message_results_parser = MessageResultsParser(HttpHeadersParser(), HttpStatusParser())
    c.alerts_parser = AlertsParser()
    c.zap_database = ZAPDatabase()
    c.zap_server = ZAPServer(c.config, c.system, c.exclude_rules_builder,
                             c.request_headers_builder, c.user_configuration_builder)

    c.zaproxy = ZAProxy(c.config.target, c.config.exclude_urls, c.zap_database, c.message_results_parser,
                        c.alerts_parser, c.config)
    c.wasc = WebApplicationSecurityConsortium()
    c.zap_log_configuration = ZAPLogConfiguration(c.config)

    c.default_scan_policy = DefaultScanPolicyFactory().build()
    c.api_scan_policy = APIScanPolicyFactory().build()
    c.active_scan_policy = ActiveScanPolicy(c.config, c.default_scan_policy, c.api_scan_policy)
    c.script_finder = ScriptFinder(c.config.script_dirs)

    c.vulnerability_report_formatter = VulnerabilityReportFormatter(c.config, c.wasc, uuid)
    c.scan_report_formatter = ScanReportFormatter(c.start_date_time, c.config)
    c.security_report_formatter = SecurityReportFormatter(c.scan_report_formatter, c.vulnerability_report_formatter,
                                                          c.config)
    c.dast_report_formatter = CombinedReportFormatter(ModifiedZapReportFormatter(c.system), c.security_report_formatter)

    c.custom_hooks = CustomHooks(c.zaproxy, c.dast_report_formatter,
                                 c.system, c.config, c.script_finder, c.active_scan_policy)
    c.scan = ScanScriptWrapper(c.config, c.zap_log_configuration,
                               c.active_scan_policy, c.zap_server, c.zaproxy, c.custom_hooks)

    Container.CACHE = c
    return Container.CACHE
