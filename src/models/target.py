from urllib.parse import urlparse


class Target(str):

    def __bool__(self) -> bool:
        if self == 'None' or self == '':
            return False

        return True

    def is_host_url(self) -> bool:
        return self.count('/') <= 2

    def reset_to_host(self) -> 'Target':
        if self.is_host_url():
            return self

        return self.__class__(self[0:self.index('/', 8)])

    def hostname(self) -> str:
        return urlparse(self).hostname

    def build_url(self, path: str) -> str:
        return f"{self}/{path.strip().strip('/')}"
