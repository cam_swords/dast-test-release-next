from __future__ import annotations

import itertools
from typing import Callable, List, Sequence

from .aggregated_alert import AggregatedAlert
from .alert import Alert, AlertProperty
from .rule import RuleID


class Alerts:

    NOISY_ALERT_CVE_VALUES = [
        '10015',  # https://www.zaproxy.org/docs/alerts/10015
        '10020',  # https://www.zaproxy.org/docs/alerts/10020
        '10025',  # https://www.zaproxy.org/docs/alerts/10025
        '10036',  # https://www.zaproxy.org/docs/alerts/10036
        '10037',  # https://www.zaproxy.org/docs/alerts/10037
        '10052',  # https://www.zaproxy.org/docs/alerts/10052
        '10053',  # https://www.zaproxy.org/docs/alerts/10053
        '10061',  # https://www.zaproxy.org/docs/alerts/10061
        '10063',  # https://www.zaproxy.org/docs/alerts/10063
        '10098',  # https://www.zaproxy.org/docs/alerts/10098
        '10107',  # https://www.zaproxy.org/docs/alerts/10107
    ]

    def __init__(self, alerts: Sequence[Alert]):
        self.__alerts = alerts

    def __len__(self) -> int:
        return len(self.__alerts)

    def __getitem__(self, key: int) -> Alert:
        return self.__alerts[key]

    def find_by_rule_id(self, rule_id: RuleID) -> Alerts:
        alerts = [alert for alert in self.__alerts
                  if alert.rule_id == rule_id]

        return Alerts(alerts)

    def sample(self, size: int) -> Alerts:
        return Alerts(self.__alerts[0:size])

    def sort_by(self, properties: List[str]) -> Callable[[List[AlertProperty]], str]:
        return lambda alert: ':'.join([getattr(alert, prop) for prop in properties])

    def sort(self) -> Alerts:
        sorted_alerts = sorted(self.__alerts, key=lambda a: (
            -int(a.riskcode),
            a.name,
            a.method,
            a.url,
            a.param))

        return Alerts(sorted_alerts)

    def aggregate(self) -> Alerts:
        aggregated_alerts_dict = {}
        for key, group in itertools.groupby(self.__alerts, lambda a: self._group_alerts_by(a)):
            aggregated_alerts_dict[key] = list(group)

        alerts = []
        for key, aggregated_alerts in aggregated_alerts_dict.items():
            rule_id = key.split('::')[0]
            if rule_id in self.NOISY_ALERT_CVE_VALUES:
                alerts.extend([AggregatedAlert(aggregated_alerts)])
            else:
                alerts.extend(aggregated_alerts)

        return Alerts(alerts)

    def _group_alerts_by(self, alert: Alert) -> str:
        return f'{alert.rule_id}::{alert.risk}::{alert.confidence}'
