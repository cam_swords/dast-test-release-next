from __future__ import annotations

from .http_request import HttpRequest
from .http_response import HttpResponse
from ..id import ID


class HttpMessageID(ID):
    pass


class HttpMessage:

    def __init__(self, message_id: str, request: HttpRequest, response: HttpResponse):
        self._message_id = message_id
        self._request = request
        self._response = response

    @property
    def message_id(self) -> HttpMessageID:
        return HttpMessageID(self._message_id)

    @property
    def request(self) -> HttpRequest:
        return self._request

    @property
    def response(self) -> HttpResponse:
        return self._response
