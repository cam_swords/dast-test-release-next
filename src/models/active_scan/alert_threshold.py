from enum import Enum


# Represents the minimum threshold required to reach to be classified as an alert.
class AlertThreshold(Enum):

    OFF = 0  # Using the threshold OFF means the rule will not execute on a ZAP scan.
    DEFAULT = 1
    LOW = 2
    MEDIUM = 3
    HIGH = 4
