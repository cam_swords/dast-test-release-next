from src.models.active_scan.alert_threshold import AlertThreshold
from src.models.active_scan.rule_configuration import RuleConfiguration
from src.models.active_scan.rule_configurations import RuleConfigurations
from src.models.active_scan.rule_strength import RuleStrength
from src.models.active_scan.scan_policy import ScanPolicy


# Builds the default active scan policy when running an API scan.
class APIScanPolicyFactory:

    def build(self):
        return ScanPolicy(file_name='API-Minimal.policy',
                          level=AlertThreshold.MEDIUM,
                          strength=RuleStrength.MEDIUM,
                          rule_configurations=self._rule_configurations())

    def _rule_configurations(self) -> RuleConfigurations:
        return RuleConfigurations([
            RuleConfiguration('6', False, AlertThreshold.OFF),
            RuleConfiguration('7', True, AlertThreshold.MEDIUM),
            RuleConfiguration('40009', True, AlertThreshold.MEDIUM),
            RuleConfiguration('40012', False, AlertThreshold.OFF),
            RuleConfiguration('40014', False, AlertThreshold.OFF),
            RuleConfiguration('40018', True, AlertThreshold.MEDIUM),
            RuleConfiguration('90019', True, AlertThreshold.MEDIUM),
            RuleConfiguration('90020', True, AlertThreshold.MEDIUM),
            RuleConfiguration('0', True, AlertThreshold.MEDIUM),
            RuleConfiguration('20019', True, AlertThreshold.MEDIUM),
            RuleConfiguration('30001', True, AlertThreshold.MEDIUM),
            RuleConfiguration('30002', True, AlertThreshold.MEDIUM),
            RuleConfiguration('40003', True, AlertThreshold.MEDIUM),
            RuleConfiguration('40008', True, AlertThreshold.MEDIUM),
            RuleConfiguration('40016', False, AlertThreshold.OFF),
            RuleConfiguration('40017', False, AlertThreshold.OFF),
            RuleConfiguration('50000', True, AlertThreshold.MEDIUM),
            RuleConfiguration('42', False, AlertThreshold.OFF),
            RuleConfiguration('10045', False, AlertThreshold.OFF),
            RuleConfiguration('10048', False, AlertThreshold.OFF),
            RuleConfiguration('20012', False, AlertThreshold.OFF),
            RuleConfiguration('20015', False, AlertThreshold.OFF),
            RuleConfiguration('20016', False, AlertThreshold.OFF),
            RuleConfiguration('20017', False, AlertThreshold.OFF),
            RuleConfiguration('20018', False, AlertThreshold.OFF),
            RuleConfiguration('40013', False, AlertThreshold.OFF),
            RuleConfiguration('40019', False, AlertThreshold.OFF),
            RuleConfiguration('40020', False, AlertThreshold.OFF),
            RuleConfiguration('40021', False, AlertThreshold.OFF),
            RuleConfiguration('40022', False, AlertThreshold.OFF),
            RuleConfiguration('90018', False, AlertThreshold.OFF),
            RuleConfiguration('90021', True, AlertThreshold.MEDIUM),
            RuleConfiguration('90023', True, AlertThreshold.MEDIUM),
            RuleConfiguration('90024', False, AlertThreshold.OFF),
            RuleConfiguration('90025', False, AlertThreshold.OFF),
            RuleConfiguration('10095', False, AlertThreshold.OFF),
            RuleConfiguration('30003', False, AlertThreshold.OFF),
            RuleConfiguration('90028', False, AlertThreshold.OFF),
            RuleConfiguration('20014', False, AlertThreshold.OFF),
            RuleConfiguration('40023', False, AlertThreshold.OFF),
            RuleConfiguration('41', False, AlertThreshold.OFF),
            RuleConfiguration('43', False, AlertThreshold.OFF),
            RuleConfiguration('10107', False, AlertThreshold.OFF),
            RuleConfiguration('40015', False, AlertThreshold.OFF),
            RuleConfiguration('40024', False, AlertThreshold.OFF),
            RuleConfiguration('40026', False, AlertThreshold.OFF),
            RuleConfiguration('60100', False, AlertThreshold.OFF),
            RuleConfiguration('60101', False, AlertThreshold.OFF),
            RuleConfiguration('90026', True, AlertThreshold.MEDIUM),
            RuleConfiguration('90029', True, AlertThreshold.MEDIUM),
            RuleConfiguration('10051', False, AlertThreshold.OFF),
            RuleConfiguration('10053', False, AlertThreshold.OFF),
            RuleConfiguration('10104', False, AlertThreshold.OFF),
            RuleConfiguration('10106', False, AlertThreshold.OFF),
            RuleConfiguration('40025', False, AlertThreshold.OFF),
            RuleConfiguration('10047', False, AlertThreshold.OFF),
            RuleConfiguration('90027', False, AlertThreshold.OFF),
        ])
