from src.models.active_scan.alert_threshold import AlertThreshold
from src.models.active_scan.rule_configurations import RuleConfigurations
from src.models.active_scan.rule_strength import RuleStrength
from src.models.active_scan.scan_policy import ScanPolicy


# Builds the default active scan policy.
class DefaultScanPolicyFactory:

    def build(self):
        return ScanPolicy(file_name='Default Policy.policy',
                          level=AlertThreshold.MEDIUM,
                          strength=RuleStrength.MEDIUM,
                          rule_configurations=RuleConfigurations())
