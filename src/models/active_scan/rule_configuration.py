from .alert_threshold import AlertThreshold


class RuleConfiguration:

    def __init__(self, rule_id: str, enabled: bool, level: AlertThreshold):
        self.rule_id = rule_id
        self.enabled = enabled
        self.level = level

    @classmethod
    def turn_off(cls, rule_id):
        return RuleConfiguration(rule_id, False, AlertThreshold.OFF)
