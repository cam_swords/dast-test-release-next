from __future__ import annotations

from typing import Callable, List, TypeVar

from .rule_configuration import RuleConfiguration

REDUCE_RETURN_TYPE = TypeVar('REDUCE_RETURN_TYPE')
REDUCE_ITERATOR_RETURN_TYPE = TypeVar('REDUCE_ITERATOR_RETURN_TYPE')


class RuleConfigurations:

    def __init__(self, rules: List[RuleConfiguration] = []):
        self.rules = rules

    def reduce(self,
               aggregator: Callable[[List[REDUCE_ITERATOR_RETURN_TYPE]], REDUCE_RETURN_TYPE],
               iterator: Callable[[RuleConfiguration], REDUCE_ITERATOR_RETURN_TYPE]) -> REDUCE_RETURN_TYPE:
        return aggregator([iterator(rule) for rule in self.rules])

    def concat(self, rule_configurations: RuleConfigurations) -> RuleConfigurations:
        return RuleConfigurations(self.rules + rule_configurations.rules)
