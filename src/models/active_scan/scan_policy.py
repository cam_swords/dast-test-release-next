from pathlib import Path
from typing import NewType

from src.models.active_scan.alert_threshold import AlertThreshold
from src.models.active_scan.rule_configurations import RuleConfigurations
from src.models.active_scan.rule_strength import RuleStrength


ScanPolicyName = NewType('ScanPolicyName', str)


# Represents a set of enabled/disabled active scan rules to be run in a scan.
class ScanPolicy:

    def __init__(self,
                 file_name: str,
                 level: AlertThreshold,
                 strength: RuleStrength,
                 rule_configurations: RuleConfigurations):
        self.__file_name = file_name
        self.__level = level
        self.__strength = strength
        self.__rule_configurations = rule_configurations

    def name(self) -> ScanPolicyName:
        file_name_without_extension = Path(self.file_name()).stem

        return ScanPolicyName(file_name_without_extension)

    def file_name(self):
        return self.__file_name

    def level(self):
        return self.__level

    def strength(self):
        return self.__strength

    def rule_configurations(self):
        return self.__rule_configurations
