from enum import Enum


# Represents the amount of checks each ZAP active scan rule should perform.
class RuleStrength(Enum):

    DEFAULT = 0
    LOW = 1
    MEDIUM = 2
    HIGH = 3
    INSANE = 4
