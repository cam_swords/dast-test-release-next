from .id import ID


class WascID(ID):

    INVALID_INT_MESSAGE = 'ID must be a non-zero integer'

    def valid(self) -> bool:
        return super().valid() and not self._id == '0'


class WebApplicationSecurityConsortium():
    REFERENCE_URLS = {
        1: 'https://projects.webappsec.org/Insufficient-Authentication',
        2: 'https://projects.webappsec.org/Insufficient-Authorization',
        3: 'https://projects.webappsec.org/Integer-Overflows',
        4: 'https://projects.webappsec.org/Insufficient-Transport-Layer-Protection',
        5: 'https://projects.webappsec.org/Remote-File-Inclusion',
        6: 'https://projects.webappsec.org/Format-String',
        7: 'https://projects.webappsec.org/Buffer-Overflow',
        8: 'https://projects.webappsec.org/Cross-Site-Scripting',
        9: 'https://projects.webappsec.org/Cross-Site-Request-Forgery',
        10: 'https://projects.webappsec.org/Denial-of-Service',
        11: 'https://projects.webappsec.org/Brute-Force',
        12: 'https://projects.webappsec.org/Content-Spoofing',
        13: 'https://projects.webappsec.org/Information-Leakage',
        14: 'https://projects.webappsec.org/Server-Misconfiguration',
        15: 'https://projects.webappsec.org/Application-Misconfiguration',
        16: 'https://projects.webappsec.org/Directory-Indexing',
        17: 'https://projects.webappsec.org/Improper-Filesystem-Permissions',
        18: 'https://projects.webappsec.org/Credential-and-Session-Prediction',
        19: 'https://projects.webappsec.org/SQL-Injection',
        20: 'https://projects.webappsec.org/Improper-Input-Handling',
        21: 'https://projects.webappsec.org/Insufficient+Anti-automation',
        22: 'https://projects.webappsec.org/Improper-Output-Handling',
        23: 'https://projects.webappsec.org/XML-Injection',
        24: 'https://projects.webappsec.org/HTTP-Request-Splitting',
        25: 'https://projects.webappsec.org/HTTP-Response-Splitting',
        26: 'https://projects.webappsec.org/HTTP-Request-Smuggling',
        27: 'https://projects.webappsec.org/HTTP-Response-Smuggling',
        28: 'https://projects.webappsec.org/Null-Byte-Injection',
        29: 'https://projects.webappsec.org/LDAP-Injection',
        30: 'https://projects.webappsec.org/Mail-Command-Injection',
        31: 'https://projects.webappsec.org/OS-Commanding',
        32: 'https://projects.webappsec.org/Routing-Detour',
        33: 'https://projects.webappsec.org/Path-Traversal',
        34: 'https://projects.webappsec.org/Predictable-Resource-Location',
        35: 'https://projects.webappsec.org/SOAP-Array-Abuse',
        36: 'https://projects.webappsec.org/SSI-Injection',
        37: 'https://projects.webappsec.org/Session-Fixation',
        38: 'https://projects.webappsec.org/URL-Redirector-Abuse',
        39: 'https://projects.webappsec.org/XPath-Injection',
        40: 'https://projects.webappsec.org/Insufficient-Process-Validation',
        41: 'https://projects.webappsec.org/XML-Attribute-Blowup',
        42: 'https://projects.webappsec.org/Abuse-of-Functionality',
        43: 'https://projects.webappsec.org/XML-External-Entities',
        44: 'https://projects.webappsec.org/XML-Entity-Expansion',
        45: 'https://projects.webappsec.org/Fingerprinting',
        46: 'https://projects.webappsec.org/XQuery-Injection',
        47: 'https://projects.webappsec.org/Insufficient-Session-Expiration',
        48: 'https://projects.webappsec.org/Insecure-Indexing',
        49: 'https://projects.webappsec.org/Insufficient-Password-Recovery',
    }

    OVERVIEW_REFERENCE = 'http://projects.webappsec.org/w/page/13246974/Threat%20Classification%20Reference%20Grid'

    def reference_url_for_id(self, wasc_id: WascID) -> str:
        if not wasc_id.valid():
            raise ValueError(f'unable to determine reference url for wasc_id {wasc_id}')

        return self.REFERENCE_URLS.get(int(wasc_id), self.OVERVIEW_REFERENCE)
