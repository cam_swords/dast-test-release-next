from typing import List

from src.configuration import Configuration


class ExcludeRulesConfigurationBuilder:

    def __init__(self, config: Configuration):
        self._config = config

    def build(self) -> List[str]:
        rules = []

        for index, rule_id in enumerate(self._config.exclude_rules or []):
            rules.extend([
                '-config', f'globalalertfilter.filters.filter({index}).ruleid={rule_id}',
                '-config', f'globalalertfilter.filters.filter({index}).url=.*',
                '-config', f'globalalertfilter.filters.filter({index}).urlregex=true',
                '-config', f'globalalertfilter.filters.filter({index}).newrisk=-1',
                '-config', f'globalalertfilter.filters.filter({index}).enabled=true',
            ])

        return rules
