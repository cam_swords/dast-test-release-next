from typing import List, Optional, Tuple

from src.models.http import HttpMessage, HttpMessages, HttpRequest, HttpResponse
from .http_status_parser import HttpStatusParser
from ..http_headers_parser import HttpHeadersParser


class MessageResultsParser:

    def __init__(self,
                 http_headers_parser: HttpHeadersParser,
                 http_status_parser: HttpStatusParser):
        self._http_headers_parser = http_headers_parser
        self._http_status_parser = http_status_parser

    def parse(self, results: List[Tuple[str, ...]]) -> HttpMessages:
        messages = []

        for result in results:
            message = self._parse_message(result)

            if message:
                messages.append(message)

        return HttpMessages(messages)

    def _parse_message(self, result: Optional[Tuple[str, ...]]) -> Optional[HttpMessage]:
        if not result or len(result) < 5:
            return None

        status_code, status_text = self._http_status_parser.parse(result[4])
        request = HttpRequest(result[1], result[2], self._http_headers_parser.parse(result[3]))
        response = HttpResponse(status_code, status_text, self._http_headers_parser.parse(result[4]))

        return HttpMessage(result[0], request, response)
