import logging
from os import linesep
from typing import Callable, Dict, List

from src.configuration import Configuration


class BrowserkerConfigurationFile:

    def __init__(self, config: Configuration, file_path: str):
        self._config = config
        self._file_path = file_path

    def write(self) -> None:
        allowed_hosts = self.list_values(self._config.browserker_allowed_hosts)
        excluded_uris = self.list_values(self._config.exclude_urls, lambda url: url != self._config.auth_url)

        settings = [
            f'AllowedHosts = [{allowed_hosts}]',
            'DataPath = "/output/browserker_data"',
            'DebugLogFile = "/output/browserker-debug.log"',
            'DebugLogLevel = "debug"',
            'DisableHeadless = false',
            f'ExcludedURIs = [{excluded_uris}]',
            f'IgnoredHosts = [{self.list_values(self._config.browserker_ignored_hosts)}]',
            f'ExcludedHosts = [{self.list_values(self._config.browserker_excluded_hosts)}]',
            'JSPluginPath = "/browserker/plugins/"',
            'LogLevel = "info"',
            f'MaxActions = {self._config.browserker_max_actions}',
            f'MaxAttackFailures = {self._config.browserker_max_attack_failures}',
            f'MaxDepth = {self._config.browserker_max_depth}',
            f'NumBrowsers = {self._config.browserker_number_of_browsers}',
            'PassiveMode = true',
            f'Proxy = "http://127.0.0.1:{self._config.zap_port}"',
            'PluginResourcePath = "/browserker/resources/"',
            f'URL = "{self._config.target}"',
        ]

        if self._config.auth_username and self._config.auth_password:
            settings.extend([
                '',
                '[FormData]',
                f'UserName = "{self._config.auth_username}"',
                f'Password = "{self._config.auth_password}"',
            ])

            if '@' in self._config.auth_username:
                settings.extend([f'Email = "{self._config.auth_username}"'])

        settings.extend(self.mapped_values('CustomHeaders', self._config.request_headers))
        settings.extend(self.mapped_values('CustomCookies', self._config.browserker_cookies))

        for setting in settings:
            logging.info(f'Adding Browserker setting {setting}')

        with open(self._file_path, 'w') as file:
            for setting in settings:
                file.write(f'{setting}{linesep}')

    def list_values(self, values_list: List[str], url_acceptable: Callable[[str], bool] = lambda url: True) -> str:
        if not values_list:
            return ''

        values = [url for url in values_list if url and url_acceptable(url)]

        if not values:
            return ''

        comma_separated = '", "'.join(values)
        return f'"{comma_separated}"'

    def mapped_values(self, name: str, values: Dict[str, str]) -> List[str]:
        if not values:
            return []

        settings = ['', f'[{name}]']

        for key, value in values.items():
            settings.extend([f'{key} = "{value}"'])

        return settings
