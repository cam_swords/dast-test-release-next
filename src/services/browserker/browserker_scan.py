import logging
import time

from src.configuration import Configuration
from src.models import Target
from src.system import System
from src.zap_gateway.zaproxy import ZAProxy
from . import BrowserkerConfigurationFile


class BrowserkerScan:
    LOG = '/output/browserker.log'
    DOT_FILE = '/output/report.dot'
    CONFIGURATION_FILE = '/tmp/scan.toml'

    def __init__(self, config: Configuration, target: Target, zaproxy: ZAProxy):
        self._config = config
        self._target = target
        self._zaproxy = zaproxy

    def run(self) -> None:
        logging.info('Creating Browserker configuration file from DAST settings')
        BrowserkerConfigurationFile(self._config, self.CONFIGURATION_FILE).write()

        self._set_zap_allowed_hosts()

        logging.info('Starting Browserker...')
        commands = ['/browserker/analyzer', 'run', '--config', self.CONFIGURATION_FILE, '--dot', self.DOT_FILE]

        process = System().run(commands, self.LOG)

        exit_code = process.poll()

        while exit_code is None:
            logging.debug('Browserker is running, wait 5 seconds')
            time.sleep(5)
            exit_code = process.poll()

        logging.info(f'Browserker completed with exit code {exit_code}, continuing scan')

    def _set_zap_allowed_hosts(self) -> None:
        hosts = '|'.join(self._config.browserker_allowed_hosts)
        only_proxy_hosts_regexp = rf'^(?:(?!https?:\/\/({hosts})).*).$'
        self._zaproxy.set_global_exclude_urls(only_proxy_hosts_regexp)
