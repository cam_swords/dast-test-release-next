import logging
import time
from typing import Optional

from src.models import ContextID, ScanPolicyName, Target
from src.zap_gateway import Settings, ZAProxy
from .active_scan_logger import ActiveScanLogger
from .active_scan_policy import ActiveScanPolicy


class ActiveScan:

    STATUS_POLLING_INTERVAL_SECONDS = 5

    def __init__(self, zap: ZAProxy, target: Target, policy: ActiveScanPolicy,
                 is_api_scan: bool):
        self._zap = zap
        self._target = target
        self._policy = policy
        self._is_api_scan = is_api_scan

    def run(self) -> None:
        logging.info(f'Active Scan {self._target} with policy {self._policy_name}')

        scan_id = self._zap.run_active_scan(self._target, self._policy_name, self._context_id)
        self._wait_for_scan(scan_id)

        logging.info('Active Scan complete')

        ActiveScanLogger(self._zap.active_scan_progress(scan_id)).log()

    @property
    def _policy_name(self) -> ScanPolicyName:
        return self._policy.active_policy().name()

    @property
    def _context_id(self) -> Optional[ContextID]:
        if self._is_api_scan:
            return None

        context = self._zap.context_details(Settings.CONTEXT_NAME)

        if context:
            context_id = context['id']

            logging.info(f'Active scan context: {context_id}')

            return context_id

    def _wait_for_scan(self, scan_id: int) -> None:
        time.sleep(self.STATUS_POLLING_INTERVAL_SECONDS)

        progress = self._zap.active_scan_percent_complete(scan_id)

        while progress < 100:
            logging.info(f'Active Scan progress: {progress}%')

            time.sleep(self.STATUS_POLLING_INTERVAL_SECONDS)

            progress = self._zap.active_scan_percent_complete(scan_id)
