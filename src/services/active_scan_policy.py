from typing import IO

from src.configuration import Configuration
from src.models.active_scan.rule_configuration import RuleConfiguration
from src.models.active_scan.rule_configurations import RuleConfigurations
from src.models.active_scan.scan_policy import ScanPolicy


class ActiveScanPolicy:

    POLICIES_DIRECTORY = '/app/zap/policies'

    def __init__(self, config: Configuration, default_scan_policy: ScanPolicy, api_scan_policy: ScanPolicy):
        self.config = config
        self.default_scan_policy = default_scan_policy
        self.api_scan_policy = api_scan_policy

    def get_path(self) -> str:
        return f'{self.POLICIES_DIRECTORY}/{self.active_policy().file_name()}'

    def write_policy(self, io: IO) -> None:
        new_line = '\n'
        excluded_rules = [RuleConfiguration.turn_off(rule_id) for rule_id in self.config.exclude_rules]
        rule_configurations = self.active_policy().rule_configurations().concat(RuleConfigurations(excluded_rules))

        output = f"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
                     <configuration>
                       <policy>{self.active_policy().name()}</policy>
                       <scanner>
                         <level>{self.active_policy().level().name}</level>
                         <strength>{self.active_policy().strength().name}</strength>
                       </scanner>
                       <plugins>
                         {rule_configurations.reduce(new_line.join, self.__build_rule)}
                       </plugins>
                     </configuration>"""

        io.write(output)

    def active_policy(self) -> ScanPolicy:
        return self.api_scan_policy if self.config.is_api_scan else self.default_scan_policy

    def __build_rule(self, rule: RuleConfiguration) -> str:
        rule_id = rule.rule_id
        enabled = str(rule.enabled).lower()
        level = rule.level.name
        return f'<p{rule_id}><enabled>{enabled}</enabled><level>{level}</level></p{rule_id}>'
