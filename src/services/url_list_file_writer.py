from typing import List


class UrlListFileWriter:

    def __init__(self, urls_to_scan: List[str], file_path: str):
        self._urls_to_scan = urls_to_scan
        self._file_path = file_path

    def write(self) -> None:
        with open(self._file_path, 'w') as filehandle:
            for url in self._urls_to_scan:
                filehandle.write('%s\n' % url)
