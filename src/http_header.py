import re


class HttpHeader:
    MASK = '********'

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def mask(self):
        if self.name.lower() == 'Set-Cookie'.lower():
            return self._mask_set_cookie()

        if self.name.lower() == 'Cookie'.lower():
            return self._mask_cookie()

        return HttpHeader(self.name, HttpHeader.MASK)

    def to_dict(self):
        return {'name': self.name, 'value': self.value}

    def _mask_set_cookie(self):
        values_parts = self.value.split(';')
        # mask the value in a name/value, e.g. sessionId=1234 -> sessionId=*****
        masked = [re.sub('(.*=).*', f'\\1{HttpHeader.MASK}', values_parts[0])]

        # add any Set-Cookie options to the masked value, e.g. HttpOnly, Secure
        if len(values_parts) > 1:
            masked = masked + values_parts[1:]

        return HttpHeader(self.name, ';'.join(masked))

    def _mask_cookie(self):
        masked = []

        for name_value in self.value.split(';'):
            # mask the value in a name/value, e.g. sessionId=1234 -> sessionId=*****
            masked.append(re.sub('(.*=).*', f'\\1{HttpHeader.MASK}', name_value))

        return HttpHeader(self.name, ';'.join(masked))
