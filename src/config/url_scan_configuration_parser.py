import os
from typing import List

from src.config.argument_type import ArgumentType
from src.models import Target
from src.zap_gateway import Settings
from .invalid_configuration_error import InvalidConfigurationError


class URLScanConfigurationParser:

    DAST_PATHS_ENV_VAR = 'DAST_PATHS'
    DAST_PATHS_FILE_ENV_VAR = 'DAST_PATHS_FILE'

    def __init__(self, target: str, paths_to_scan_list: List[str], paths_to_scan_file_path: str, target_env_var: str):
        self._target = Target(target).reset_to_host()
        self._paths_to_scan_list = paths_to_scan_list
        self._paths_to_scan_file_path = paths_to_scan_file_path
        self._target_env_var = target_env_var

    def parse(self) -> List[str]:
        if self._paths_to_scan_list and not self._target:
            raise InvalidConfigurationError(f'When {self.DAST_PATHS_ENV_VAR} is defined {self._target_env_var} '
                                            'must also be set')

        if self._paths_to_scan_file_path and not self._target:
            raise InvalidConfigurationError(
                f'When {self.DAST_PATHS_FILE_ENV_VAR} is defined {self._target_env_var} must also be set')

        if self._paths_to_scan_list and self._paths_to_scan_file_path:
            raise InvalidConfigurationError(f'{self.DAST_PATHS_ENV_VAR} and {self.DAST_PATHS_FILE_ENV_VAR} '
                                            'can not be defined at the same time')

        if self._paths_to_scan_list:
            return self._build_urls(self._paths_to_scan_list)

        if self._paths_to_scan_file_path:
            if not self._paths_to_scan_file_path_valid():
                raise InvalidConfigurationError(f'{self._full_file_path} can not be found.')

            with open(self._full_file_path, 'r') as filehandle:
                list_of_paths = filehandle.readlines()
                return self._build_urls(list_of_paths)

        return []

    @property
    def _full_file_path(self) -> str:
        return os.path.realpath(f'{Settings.WRK_DIR}{self._paths_to_scan_file_path}')

    def _build_urls(self, path_list: List[str]) -> List[str]:
        return [ArgumentType.url(self._target.build_url(path)) for path in path_list]

    def _paths_to_scan_file_path_valid(self) -> bool:
        return os.path.exists(self._full_file_path) and self._full_file_path.startswith(Settings.WRK_DIR)
