from __future__ import annotations

from argparse import ArgumentParser, ArgumentTypeError, RawDescriptionHelpFormatter
from collections import namedtuple
from os import path
from typing import Callable, Dict, List, MutableMapping

from src.config.argument_type import ArgumentType
from src.models.target import Target
from src.zap_gateway import Settings
from .fallback_to_environment import FallbackToEnvironment
from .invalid_configuration_error import InvalidConfigurationError
from .url_scan_configuration_parser import URLScanConfigurationParser
from .. import Configuration
from .. import System


BrowserkerOption = namedtuple('BrowserkerOption', 'name type nargs default help')


class ConfigurationParser:
    AJAX_SPIDER_COMMAND_ARG = '--use-ajax-spider'
    AJAX_SPIDER_COMMAND_ARG_ABBR = '-j'
    AJAX_SPIDER_ENV_VAR = 'DAST_USE_AJAX_SPIDER'
    AJAX_SPIDER_ZAP_ENV_VAR = 'DAST_ZAP_USE_AJAX_SPIDER'

    API_SPEC_ENV_VAR = 'DAST_API_SPECIFICATION'
    API_SPEC_COMMAND_ARG = '--api-specification'

    DAST_API_SCAN_SCRIPTS_DIR = '/app/resources/scripts/api_scan'
    DAST_NORMAL_SCAN_SCRIPTS_DIR = '/app/resources/scripts/normal_scan'
    DAST_PATHS_ENV_VAR = URLScanConfigurationParser.DAST_PATHS_ENV_VAR
    DAST_PATHS_FILE_ENV_VAR = URLScanConfigurationParser.DAST_PATHS_FILE_ENV_VAR
    IS_FUTURE_BUILD_FILE = '/app/building_for.future'

    TARGET_ENV_VAR = 'DAST_WEBSITE'

    GITLAB_EXCLUDE_RULES = ['10096']

    def parse(self, argv: List[str], environment: MutableMapping) -> Configuration:
        description = """
Run GitLab DAST against a target website of your choice.

Arguments can be supplied via command line and will fallback to using the outlined environment variable.
"""

        parser = ArgumentParser(description=description,
                                formatter_class=RawDescriptionHelpFormatter)

        # DAST configuration options
        parser.add_argument('-t', dest='target', action=FallbackToEnvironment, environment=environment,
                            type=self._empty_or_url,
                            environment_variables=[self.TARGET_ENV_VAR],
                            help='The URL of the website to scan')
        parser.add_argument('--spider-start-at-host', dest='spider_start_at_host',
                            action=FallbackToEnvironment, environment=environment, type=self._truthy, default=True,
                            environment_variables=['DAST_SPIDER_START_AT_HOST'],
                            help='Reset the target to its host before scanning')
        parser.add_argument('--paths-to-scan', dest='paths_to_scan_list', action=FallbackToEnvironment,
                            environment=environment, type=self._list_of_strings,
                            default=[],
                            environment_variables=[self.DAST_PATHS_ENV_VAR],
                            help='A list of URL paths to scan. '
                                 'The target is prepended to each path')
        parser.add_argument('--paths-to-scan-file', dest='paths_to_scan_file_path', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[self.DAST_PATHS_FILE_ENV_VAR],
                            help='The file that lists the URL paths to scan. '
                                 'The target is prepended to each path')
        parser.add_argument(self.API_SPEC_COMMAND_ARG, dest='api_specification', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[self.API_SPEC_ENV_VAR],
                            help='The file or URL of the API specification')
        parser.add_argument('--auth-url', dest='auth_url', type=ArgumentType.url, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_URL', 'AUTH_URL'],
                            help='login form URL')
        parser.add_argument('--auth-verification-url', dest='auth_verification_url',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_VERIFICATION_URL'],
                            type=self._empty_or_url,
                            default='',
                            help='A URL that is only accessible when the user is logged in, for verifying that '
                                 'authentication was successful. If this argument is present, the scan will '
                                 'exit if the URL cannot be accessed')
        parser.add_argument('--auth-username', dest='auth_username', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME', 'AUTH_USERNAME'],
                            help='login form username')
        parser.add_argument('--auth-password', dest='auth_password', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD', 'AUTH_PASSWORD'],
                            help='login form password')
        parser.add_argument('--auth-username-field', dest='auth_username_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME_FIELD', 'AUTH_USERNAME_FIELD'],
                            help='login form id or name of the username input field')
        parser.add_argument('--auth-password-field', dest='auth_password_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD_FIELD', 'AUTH_PASSWORD_FIELD'],
                            help='login form id or name of the password input field')
        parser.add_argument('--auth-submit-field', dest='auth_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SUBMIT_FIELD', 'AUTH_SUBMIT_FIELD'],
                            help='login form id or name of submit input')
        parser.add_argument('--auth-first-submit-field', dest='auth_first_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FIRST_SUBMIT_FIELD', 'AUTH_FIRST_SUBMIT_FIELD'],
                            help='login form id or name of submit input of first page')
        parser.add_argument('--auth-display', dest='auth_display', type=bool, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_DISPLAY', 'AUTH_DISPLAY'],
                            help='set the virtual display to be visible. '
                                 'This option is deprecated and will be removed in GitLab 14.0.')
        parser.add_argument('--auth-auto', dest='auth_auto', type=bool, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_AUTO', 'AUTH_AUTO'],
                            help='login for automatically finds login fields')
        parser.add_argument('--auth-exclude-urls', dest='exclude_urls', type=self._list_of_urls,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=[],
                            environment_variables=['DAST_EXCLUDE_URLS', 'DAST_AUTH_EXCLUDE_URLS', 'AUTH_EXCLUDE_URLS'],
                            help='comma separated list of URLs to exclude, no spaces in between, supply all ' +
                                 'URLs causing logout')
        parser.add_argument('--request-headers', dest='request_headers',
                            type=self._dict_of_key_values('request headers'),
                            action=FallbackToEnvironment,
                            environment=environment,
                            default={},
                            environment_variables=['DAST_REQUEST_HEADER', 'DAST_REQUEST_HEADERS'],
                            help='comma separated list of name: value request headers, '
                                 'these will be added to every request made by ZAProxy e.g. "Cache-control: no-cache"')
        parser.add_argument('--mask-http-headers', dest='http_headers_to_mask', type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_MASK_HTTP_HEADERS'],
                            help='comma separated list of header names to be masked when exposed as evidence. This'
                                 ' is recommended for headers whose values may contain secrets')
        parser.add_argument('--exclude-rules', dest='user_exclude_rules', type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=[],
                            environment_variables=['DAST_EXCLUDE_RULES'],
                            help='comma separated list of ZAP addon IDs to exclude from the scan')
        parser.add_argument('--full-scan',
                            dest='full_scan',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FULL_SCAN_ENABLED'],
                            help='Run a ZAP Full Scan: https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan')
        parser.add_argument('--auto-update-addons',
                            dest='auto_update_addons',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTO_UPDATE_ADDONS'],
                            help='Auto-update ZAP addons before running a scan')
        parser.add_argument('--write-addons-to-update-file',
                            dest='write_addons_to_update_file',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[],
                            help=f'Write the addons that are available for update to {Settings.WRK_DIR}addons.json')
        parser.add_argument('--validate-domain',
                            dest='full_scan_domain_validation_required',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED'],
                            help='Checks the domain has the required headers for running a full-scan:' +
                                 'https://docs.gitlab.com/ee/user/application_security/dast/index.html' +
                                 '#domain-validation')
        parser.add_argument('--availability-timeout',
                            dest='availability_timeout',
                            type=int,
                            default=60,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_TARGET_AVAILABILITY_TIMEOUT'],
                            help='Time limit in seconds to wait for target availability')

        parser.add_argument('--skip-target-check', dest='skip_target_check', type=self._truthy,
                            default=False,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SKIP_TARGET_CHECK'],
                            help='Skip check that confirms the target is accessible')

        parser.add_argument('--script-dirs', dest='script_dirs', type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=[],
                            environment_variables=['DAST_SCRIPT_DIRS'],
                            help='Base directory from where custom ZAP scripts should be loaded')

        parser.add_argument('--zap-max-connection-attempts',
                            dest='zap_max_connection_attempts',
                            type=self._positive_int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_MAX_CONNECTION_ATTEMPTS'],
                            help='The amount of connection attempts to make to the ZAP API before aborting')

        parser.add_argument('--zap-connect-sleep-seconds',
                            dest='zap_connect_sleep_seconds',
                            type=self._positive_int,
                            default=1,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_CONNECT_SLEEP_SECONDS'],
                            help='The amount of seconds to wait between each connection attempt to the ZAP API')

        parser.add_argument('--passive-scan-max-wait-time',
                            dest='passive_scan_max_wait_time',
                            type=self._positive_int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSIVE_SCAN_MAX_WAIT_TIME'],
                            help='The amount of minutes to wait for the passive scan to complete')

        parser.add_argument('--aggregate-vulnerabilities',
                            dest='aggregate_vulnerabilities',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[],
                            help='Group vulnerabilities where the fix is likely to occur in one or few places')

        # Browserker configuration options
        browserker_options = [
            BrowserkerOption('scan', self._truthy, 0, False,
                             'run a Browserker scan instead of a ZAP spider scan'),
            BrowserkerOption('allowed_hosts', self._list_of_strings, '?', [],
                             'pages on these hosts are considered in scope when crawled'),
            BrowserkerOption('excluded_hosts', self._list_of_strings, '?', [],
                             'pages on these hosts are forcibly dropped when crawled'),
            BrowserkerOption('ignored_hosts', self._list_of_strings, '?', [],
                             'pages on these hosts are accessed but are not reported against'),
            BrowserkerOption('max_actions', int, '?', 10000, 'maximum number of actions to crawl in a scan'),
            BrowserkerOption('max_attack_failures', int, '?', 5,
                             'maximum number of errors encountered before a path is removed from testing'),
            BrowserkerOption('max_depth', int, '?', 10, 'maximum distance of paths that are traversed'),
            BrowserkerOption('number_of_browsers', int, '?', 3, 'number of browsers to use to crawl the target site'),
            BrowserkerOption('cookies', self._dict_of_key_values('cookies'), '?', {},
                             'comma separated list of cookie names/values added to every request'),
        ]

        for option in browserker_options:
            parser.add_argument(f"--browserker-{option.name.replace('_', '-')}",
                                dest=f'browserker_{option.name}',
                                type=option.type,
                                nargs=option.nargs,
                                default=option.default,
                                const=option.default,
                                action=FallbackToEnvironment,
                                environment=environment,
                                environment_variables=[f'DAST_BROWSERKER_{option.name.upper()}'],
                                help=f'{option.help}. This is an Alpha Browserker feature and may '
                                     'change significantly in future releases.')

        # ZAP configuration options
        parser.add_argument('-O', dest='zap_api_host_override',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_API_HOST_OVERRIDE'],
                            help='zap: Overrides the hostname defined in the API specification')
        parser.add_argument('-m',
                            '--spider-mins',
                            dest='spider_mins',
                            type=int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SPIDER_MINS'],
                            help='the number of minutes to spider for')
        parser.add_argument('-r',
                            '--html-report',
                            dest='zap_report_html',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_HTML_REPORT'],
                            help='file to write the full ZAP HTML report')
        parser.add_argument('-w',
                            '--markdown-report',
                            dest='zap_report_md',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_MARKDOWN_REPORT'],
                            help='file to write the full ZAP Markdown report')
        parser.add_argument('-x',
                            '--xml-report',
                            dest='zap_report_xml',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_XML_REPORT'],
                            help='file to write the full ZAP XML report')
        parser.add_argument('-a',
                            '--include-alpha-vulnerabilities',
                            dest='zap_include_alpha',
                            type=self._truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_INCLUDE_ALPHA_VULNERABILITIES'],
                            help='include the alpha passive and active vulnerability definitions')
        parser.add_argument('-d',
                            dest='zap_debug',
                            type=self._truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_DEBUG'],
                            help='zap: show debug messages')
        parser.add_argument('-P',
                            dest='zap_port',
                            type=int,
                            default=System().get_free_port(),
                            help='The port used by the ZAP API to listen for requests')
        parser.add_argument('-D', dest='zap_delay_in_seconds',
                            help='zap: delay in seconds to wait for passive scanning. '
                                 'NOTE: this option is unsupported and does not work. '
                                 'It will be removed in GitLab 14.0')
        parser.add_argument('-i', dest='zap_default_info', action='store_true',
                            help='zap: default rules not in the config file to INFO')
        parser.add_argument('-I', dest='zap_no_fail_on_warn', action='store_true',
                            help='zap: do not return failure on warning')
        parser.add_argument(self.AJAX_SPIDER_COMMAND_ARG_ABBR,
                            self.AJAX_SPIDER_COMMAND_ARG,
                            dest='zap_use_ajax_spider',
                            type=self._truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[self.AJAX_SPIDER_ENV_VAR, self.AJAX_SPIDER_ZAP_ENV_VAR],
                            help='use the AJAX spider in addition to the ZAP spider, useful for crawling '
                                 'sites that require JavaScript')
        parser.add_argument('-l', dest='zap_min_level',
                            help='zap: minimum level to show: PASS, IGNORE, INFO, WARN or FAIL, use '
                                 + 'with -s to hide example URLs')
        parser.add_argument('-n', dest='zap_context_file',
                            help='zap: context file which will be loaded prior to spidering the target'
                                 'NOTE: this option is unsupported and does not work. '
                                 'It will be removed in GitLab 14.0')
        parser.add_argument('-p', dest='zap_progress_file',
                            help='zap: progress file which specifies issues that are being addressed'
                                 'NOTE: this option is unsupported and does not work. '
                                 'It will be removed in GitLab 14.0')
        parser.add_argument('-s', dest='zap_short_format', action='store_true',
                            help='zap: short output format - do not show PASSes or example URLs. '
                                 'NOTE: this option is unsupported and does not work. '
                                 'It will be removed in GitLab 14.0')
        parser.add_argument('-z',
                            dest='zap_other_options',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_CLI_OPTIONS'],
                            help='zap: ZAP command line options e.g. -z"-config aaa=bbb -config ccc=ddd"')
        parser.add_argument('--zap-log-configuration',
                            dest='zap_log_configuration',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_LOG_CONFIGURATION'],
                            help='ZAP additional log configuration e.g.'
                                 '--zap_log_configuration "log4j.logger.org.parosproxy.paros.network.HttpSender=DEBUG"')
        parser.add_argument('-T',
                            dest='zap_timeout',
                            type=self._positive_int,
                            help='Sets the timeout in minutes determining how long to wait while connecting to ZAP, '
                                 'and how long to wait for the passive scan to complete. '
                                 'This option is deprecated and will be removed in GitLab 14.0. '
                                 'Please use --zap-max-connection-attempts and --passive-scan-max-wait-time instead.')

        values = parser.parse_args(argv)

        values.urls_to_scan = []
        if values.paths_to_scan_list or values.paths_to_scan_file_path:
            values.urls_to_scan = URLScanConfigurationParser(
                values.target, values.paths_to_scan_list, values.paths_to_scan_file_path, self.TARGET_ENV_VAR).parse()

        if values.zap_max_connection_attempts is None \
                and values.passive_scan_max_wait_time is None \
                and values.zap_timeout is not None:
            values.zap_max_connection_attempts = int(values.zap_timeout * 60 / (values.zap_connect_sleep_seconds or 1))
            values.passive_scan_max_wait_time = values.zap_timeout

        if values.zap_max_connection_attempts is None:
            values.zap_max_connection_attempts = 600

        if values.passive_scan_max_wait_time is None:
            values.passive_scan_max_wait_time = 600

        if values.auto_update_addons is None:
            values.auto_update_addons = False

        values.silent = self._is_silent(values)

        values.is_api_scan = False if values.api_specification is None else True

        dast_scripts_dir = self.DAST_API_SCAN_SCRIPTS_DIR if values.is_api_scan else self.DAST_NORMAL_SCAN_SCRIPTS_DIR
        values.script_dirs.append(dast_scripts_dir)

        if values.spider_mins is None:
            # a time of 0 represents unlimited maximum duration
            values.spider_mins = 0 if values.full_scan else 1

        # not set, use a sensible default
        if not values.http_headers_to_mask:
            values.http_headers_to_mask = ['Authorization', 'Proxy-Authorization', 'Set-Cookie', 'Cookie']

        # deliberately remove all headers
        if len(values.http_headers_to_mask) == 1 and not values.http_headers_to_mask[0]:
            values.http_headers_to_mask = []

        if not values.target and not values.api_specification and not values.write_addons_to_update_file:
            raise InvalidConfigurationError(
                f'Either {self.TARGET_ENV_VAR} or {self.API_SPEC_ENV_VAR} must be set')

        if values.is_api_scan and values.zap_use_ajax_spider:
            raise InvalidConfigurationError(
                f'The AJAX Spider (configured with {self.AJAX_SPIDER_ENV_VAR}, {self.AJAX_SPIDER_ZAP_ENV_VAR}, '
                f'{self.AJAX_SPIDER_COMMAND_ARG}, or {self.AJAX_SPIDER_COMMAND_ARG_ABBR}) cannot be used with '
                f'an API scan (configured with {self.API_SPEC_ENV_VAR} or {self.API_SPEC_COMMAND_ARG})',
            )

        values.browserker_all_allowed_hosts = values.browserker_allowed_hosts + [Target(values.target).hostname()]

        if values.zap_use_ajax_spider and values.browserker_scan:
            raise InvalidConfigurationError('Browserker cannot be used with an AJAX spider scan')

        if values.is_api_scan and values.browserker_scan:
            raise InvalidConfigurationError('Browserker cannot be used with an API scan')

        if values.paths_to_scan_list and values.browserker_scan:
            raise InvalidConfigurationError('Browserker cannot be used when a list of paths to scan is provided')

        if values.paths_to_scan_file_path and values.browserker_scan:
            raise InvalidConfigurationError('Browserker cannot be used when a file of paths to scan is provided')

        values.exclude_rules = values.user_exclude_rules + self.GITLAB_EXCLUDE_RULES
        values.next_major_release = path.isfile(self.IS_FUTURE_BUILD_FILE)

        return Configuration(values)

    @classmethod
    def _truthy(cls, value):
        return value == 'true' or value == 'True' or value == '1'

    @classmethod
    def _list_of_urls(cls, values):
        urls = [url.strip() for url in values.split(',')]
        return list(map(ArgumentType.url, urls))

    @classmethod
    def _list_of_strings(cls, values):
        return list([value.strip() for value in values.split(',')])

    @classmethod
    def _dict_of_key_values(cls, argument_name: str) -> Callable[[str], Dict[str, str]]:
        def type_parser(values):
            headers = [cls._dict_from_string(argument_name, name_value.strip())
                       for name_value in values.split(',')]
            return {name: value for header in headers for name, value in header.items()}

        return type_parser

    @classmethod
    def _dict_from_string(cls, argument_name: str, value: str) -> Dict[str, str]:
        parts = value.split(':')

        if len(parts) != 2:
            raise ArgumentTypeError(f'Failed to parse {argument_name}, aborting')

        return {parts[0].strip(): parts[1].strip()}

    @classmethod
    def _is_silent(cls, values):
        if '-silent' in (values.zap_other_options or '') or values.auto_update_addons is False:
            return True

        return False

    @classmethod
    def _positive_int(cls, value):
        number = int(value)

        if number < 0:
            raise ArgumentTypeError('cannot be negative')

        return number

    @classmethod
    def _empty_or_url(cls, value: str) -> str:
        if value:
            return ArgumentType.url(value)

        return value
