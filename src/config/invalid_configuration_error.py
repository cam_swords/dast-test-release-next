
class InvalidConfigurationError(Exception):
    DAST_CONFIGURATION_DOCS = 'https://docs.gitlab.com/ee/user/application_security/dast/#configuration'

    def __init__(self, message: str):
        super(InvalidConfigurationError, self).__init__(f'{message}. '
                                                        f'See {self.DAST_CONFIGURATION_DOCS} for more details.')
