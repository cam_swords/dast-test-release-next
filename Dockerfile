ARG BASE_IMAGE=registry.gitlab.com/gitlab-org/security-products/analyzers/dast/browserker:0.0.16

FROM $BASE_IMAGE as compiled_dependencies
ARG DEBIAN_FRONTEND=noninteractive
USER root
RUN apt-get update && \
    apt-get install --assume-yes --no-install-recommends \
        gcc \
        python3-dev \
        python3-pip \
        python-is-python3 \
        && \
    apt-get clean && \
        rm -rf /var/lib/apt/lists/*

RUN pip3 install memory_profiler matplotlib

FROM $BASE_IMAGE
ARG BUILDING_FOR=now
ARG DEBIAN_FRONTEND=noninteractive
ARG FIREFOX_VERSION=81.0
ARG GECKODRIVER_VERSION=0.27.0
ARG ZAP_WEEKLY_VERSION=2020-06-30
ARG COMMUNITY_SCRIPTS_URL=https://raw.githubusercontent.com/zaproxy/community-scripts/f95c690c4be594db79b5ff5f27cae2ef5e2da396/
ARG ZAP_POLICIES_URL=https://raw.githubusercontent.com/zaproxy/zaproxy/efb404d38280dc9ecf8f88c9b0c658385861bdcf/docker/policies/

USER root

# Install Python and Java
# Install Firefox (plus dependencies), remove Firefox (leaving dependencies), download specific Firefox version
RUN apt-get update && \
    apt-get install --assume-yes --no-install-recommends \
        openjdk-11-jdk \
        wget \
        curl \
        ca-certificates \
        unzip \
        python3-pip \
        python-is-python3 \
        firefox && \
    apt-get --assume-yes remove firefox && \
    cd /opt && \
    wget http://ftp.mozilla.org/pub/firefox/releases/$FIREFOX_VERSION/linux-x86_64/en-US/firefox-$FIREFOX_VERSION.tar.bz2 && \
    tar -xvjf firefox-$FIREFOX_VERSION.tar.bz2 && \
    rm firefox-$FIREFOX_VERSION.tar.bz2 && \
    ln -s /opt/firefox/firefox /usr/bin/firefox && \
    apt-get clean && \
        rm -rf /var/lib/apt/lists/*

# Install Geckodriver
RUN cd /opt && \
    wget https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && \
    tar -xvzf geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && \
    rm geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && \
    chmod +x geckodriver && \
    ln -s /opt/geckodriver /usr/bin/geckodriver && \
    export PATH=$PATH:/usr/bin/geckodriver

ARG BROWSERKER_UID=1000
ARG ZAP_UID=1001

# Install ZAP
## The following addons are installed by ZAP, yet no addon URL can be found. Versions of these addons will not be pinned.
## accessControl:6.0.0, formhandler:3.0.0, plugnhack:12.0.0, portscan:9.0.0, sequence:6.0.0.
RUN usermod --uid $BROWSERKER_UID gitlab && \
    useradd --uid $ZAP_UID --create-home --shell /usr/bin/bash zap && \
    wget https://github.com/zaproxy/zaproxy/releases/download/w$ZAP_WEEKLY_VERSION/ZAP_WEEKLY_D-$ZAP_WEEKLY_VERSION.zip && \
    unzip ZAP_WEEKLY_D-$ZAP_WEEKLY_VERSION.zip && \
    mv ZAP_D-$ZAP_WEEKLY_VERSION /zap && \
    rm ZAP_WEEKLY_D-$ZAP_WEEKLY_VERSION.zip && \
    cd /zap/plugin && \
    rm -f alertFilters-* && wget https://github.com/zaproxy/zap-extensions/releases/download/alertFilters-v10/alertFilters-release-10.zap && \
    rm -f ascanrules-* && wget https://github.com/zaproxy/zap-extensions/releases/download/ascanrules-v37/ascanrules-release-37.zap && \
    rm -f ascanrulesBeta-* && wget https://github.com/zaproxy/zap-extensions/releases/download/ascanrulesBeta-v32/ascanrulesBeta-beta-32.zap && \
    rm -f bruteforce-* && wget https://github.com/zaproxy/zap-extensions/releases/download/bruteforce-v9/bruteforce-beta-9.zap && \
    rm -f commonlib-* && wget https://github.com/zaproxy/zap-extensions/releases/download/commonlib-v1.1.0/commonlib-release-1.1.0.zap && \
    rm -f diff-* && wget https://github.com/zaproxy/zap-extensions/releases/download/diff-v10/diff-beta-10.zap && \
    rm -f directorylistv1-* && wget https://github.com/zaproxy/zap-extensions/releases/download/directorylistv1-v4/directorylistv1-release-4.zap && \
    rm -f fuzz-* && wget https://github.com/zaproxy/zap-extensions/releases/download/fuzz-v13.0.1/fuzz-beta-13.0.1.zap && \
    rm -f fuzzdb-* && wget https://github.com/zaproxy/zap-extensions/releases/download/fuzzdb-v7/fuzzdb-release-7.zap && \
    rm -f gettingStarted-* && wget https://github.com/zaproxy/zap-extensions/releases/download/gettingStarted-v11/gettingStarted-release-11.zap && \
    rm -f help-* && wget https://github.com/zaproxy/zap-core-help/releases/download/help-v10/help-release-10.zap && \
    rm -f hud-* && wget https://github.com/zaproxy/zap-hud/releases/download/v0.12.0/hud-beta-0.12.0.zap && \
    rm -f importurls-* && wget https://github.com/zaproxy/zap-extensions/releases/download/importurls-v7/importurls-beta-7.zap && \
    rm -f invoke-* && wget https://github.com/zaproxy/zap-extensions/releases/download/invoke-v10/invoke-beta-10.zap && \
    rm -f onlineMenu-* && wget https://github.com/zaproxy/zap-extensions/releases/download/onlineMenu-v7/onlineMenu-release-7.zap && \
    rm -f openapi-* && wget https://github.com/zaproxy/zap-extensions/releases/download/openapi-v16/openapi-beta-16.zap && \
    rm -f pscanrules-* && wget https://github.com/zaproxy/zap-extensions/releases/download/pscanrules-v30/pscanrules-release-30.zap && \
    rm -f pscanrulesBeta-* && wget https://github.com/zaproxy/zap-extensions/releases/download/pscanrulesBeta-v23/pscanrulesBeta-beta-23.zap && \
    rm -f quickstart-* && wget https://github.com/zaproxy/zap-extensions/releases/download/quickstart-v28/quickstart-release-28.zap && \
    rm -f replacer-* && wget https://github.com/zaproxy/zap-extensions/releases/download/replacer-v8/replacer-beta-8.zap && \
    rm -f retire-* && wget https://github.com/zaproxy/zap-extensions/releases/download/retire-v0.5.0/retire-release-0.5.0.zap && \
    rm -f reveal-* && wget https://github.com/zaproxy/zap-extensions/releases/download/reveal-v3/reveal-release-3.zap && \
    rm -f saverawmessage-* && wget https://github.com/zaproxy/zap-extensions/releases/download/saverawmessage-v5/saverawmessage-release-5.zap && \
    rm -f savexmlmessage-* && wget https://github.com/zaproxy/zap-extensions/releases/download/savexmlmessage-v0.1.0/savexmlmessage-alpha-0.1.0.zap && \
    rm -f scripts-* && wget https://github.com/zaproxy/zap-extensions/releases/download/scripts-v26/scripts-beta-26.zap && \
    rm -f selenium-* && wget https://github.com/zaproxy/zap-extensions/releases/download/selenium-v15.2.0/selenium-release-15.2.0.zap && \
    rm -f spiderAjax-* && wget https://github.com/zaproxy/zap-extensions/releases/download/spiderAjax-v23.2.0/spiderAjax-release-23.2.0.zap && \
    rm -f tips-* && wget https://github.com/zaproxy/zap-extensions/releases/download/tips-v7/tips-beta-7.zap && \
    rm -f webdriverlinux-* && wget https://github.com/zaproxy/zap-extensions/releases/download/webdriverlinux-v23/webdriverlinux-release-23.zap && \
    rm -f webdrivermacos-* && wget https://github.com/zaproxy/zap-extensions/releases/download/webdrivermacos-v22/webdrivermacos-release-22.zap && \
    rm -f webdriverwindows-* && wget https://github.com/zaproxy/zap-extensions/releases/download/webdriverwindows-v23/webdriverwindows-release-23.zap && \
    rm -f websocket-* && wget https://github.com/zaproxy/zap-extensions/releases/download/websocket-v22/websocket-release-22.zap && \
    rm -f zest-* && wget https://github.com/zaproxy/zap-extensions/releases/download/zest-v33/zest-beta-33.zap && \
    chown -R zap:zap /zap

# Download scripts and policies
ADD $COMMUNITY_SCRIPTS_URL/httpsender/Alert%20on%20HTTP%20Response%20Code%20Errors.js /app/resources/scripts/api_scan/httpsender/Alert_on_HTTP_Response_Code_Errors.js
ADD $COMMUNITY_SCRIPTS_URL/httpsender/Alert%20on%20Unexpected%20Content%20Types.js /app/resources/scripts/api_scan/httpsender/Alert_on_Unexpected_Content_Types.js
ADD ["$ZAP_POLICIES_URL/API-Minimal.policy",\
     "$ZAP_POLICIES_URL/Default%20Policy.policy",\
     "$ZAP_POLICIES_URL/St-High-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-High-Th-Low.policy",\
     "$ZAP_POLICIES_URL/St-High-Th-Med.policy",\
     "$ZAP_POLICIES_URL/St-Ins-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-Ins-Th-Low.policy",\
     "$ZAP_POLICIES_URL/St-Ins-Th-Med.policy",\
     "$ZAP_POLICIES_URL/St-Low-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-Low-Th-Low.policy",\
     "$ZAP_POLICIES_URL/St-Low-Th-Med.policy",\
     "$ZAP_POLICIES_URL/St-Med-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-Med-Th-Low.policy",\
     "/app/zap/policies/"]

# Install DAST dependencies
COPY requirements.txt /dast-requirements.txt

# Install zapcli/owasp zap seperately from requirements because dependencies are incompatible
COPY --from=compiled_dependencies --chown=root:root /usr/local/bin/mprof /usr/local/bin/mprof
COPY --from=compiled_dependencies --chown=root:staff /usr/local/lib/python3.9/dist-packages /usr/local/lib/python3.9/dist-packages
RUN pip install --no-cache zapcli python-owasp-zap-v2.4 && \
    pip install --no-cache -r /dast-requirements.txt

# Setup the DAST application
COPY profiling /app/profiling
COPY scripts /app/scripts
COPY resources /app/resources
COPY src /app/src
COPY analyze.py README.md CHANGELOG.md LICENSE /app/
COPY analyze /analyze

# Create the work directories, grant user access
# non-zap users should be able to write to work directories (/output, /zap/wrk)
WORKDIR /output
RUN touch /app/building_for.$BUILDING_FOR && \
    chown -R zap:zap /output && \
    chown -R zap:zap /app && \
    chmod 777 /output && \
    chmod 777 /zap && \
    chmod 777 /app/zap && \
    chmod 777 /app/zap/policies && \
    chmod 666 /app/zap/policies/*.policy && \
    find /app/resources -name '*.js' -exec chmod 644 {} \;

## Run as zap, running as root is not supported
USER zap
WORKDIR /output

ENTRYPOINT []
CMD ["/analyze"]
