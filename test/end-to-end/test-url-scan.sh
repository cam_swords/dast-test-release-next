#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null

  docker run \
    --name nginx \
    -v "${PWD}/fixtures/basic-multi-page-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/basic-multi-page-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
    --network test -d nginx:1.17.6 >/dev/null

  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_url_baseline_scan() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_PATHS=/page1.html,/page2.html \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_baseline_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_url_baseline_scan.json

  diff -u <(./normalize_dast_report.py expect/test_url_baseline_scan.json) \
          <(./normalize_dast_report.py output/report_test_url_baseline_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_url_full_scan() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_PATHS=page1.html,/page2.html,/page4.html \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_AUTH_EXCLUDE_URLS=http://nginx/page4.html \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_full_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_url_full_scan.json

  diff -u <(./normalize_dast_report.py expect/test_url_full_scan.json) \
          <(./normalize_dast_report.py output/report_test_url_full_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_url_scan_using_file_path() {
  docker run --rm \
    -v "${PWD}/fixtures/url-scan":/zap/wrk \
    --network test \
    --env DAST_PATHS_FILE=paths_to_scan.txt \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_file_baseline_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < "${PWD}/fixtures/url-scan/gl-dast-report.json" > output/report_test_url_file_baseline_scan.json

  diff -u <(./normalize_dast_report.py expect/test_url_file_baseline_scan.json) \
          <(./normalize_dast_report.py output/report_test_url_file_baseline_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}
