#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null

  docker run \
    --name pancakes \
    --network test \
    -v "${PWD}/fixtures/pancakes":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/pancakes/nginx.conf":/etc/nginx/conf.d/default.conf \
    -d nginx:1.17.6-alpine >/dev/null

  true
}

teardown_suite() {
  docker rm --force pancakes  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_scan() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_EXCLUDE_RULES=10096,10050,10027 \
    --env DAST_BROWSERKER_SCAN=true \
    --env DAST_BROWSERKER_EXCLUDED_HOSTS="fonts.googleapis.com" \
    --env DAST_BROWSERKER_IGNORED_HOSTS="unpkg.com" \
    --env DAST_BROWSERKER_COOKIES="dast_scan: browserker" \
    --env DAST_MASK_HTTP_HEADERS="" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://pancakes >output/test_browserker_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . <gl-dast-report.json >output/report_test_browserker_scan.json
  rm -rf ./browserker_data
  mv ./browserker.log output/report_test_browserker_scan_browserker.log
  mv ./browserker-debug.log output/report_test_browserker_scan_debug.log
  mv ./report.dot output/report_test_browserker_scan_report.dot
  mv ./findings.json output/report_test_browserker_scan_findings.json

  diff -u <(./normalize_dast_report.py expect/test_browserker_scan.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  grep -q "Adding Browserker setting AllowedHosts = \[\"pancakes\"]" output/test_browserker_scan.log &&
  grep -q "Crawled path.*LoadURL \[http://pancakes]" output/report_test_browserker_scan_browserker.log
  assert_equals "0" "$?" "Browserker log output different than expected"

  grep -q "dast_scan=browserker" output/report_test_browserker_scan.json
  assert_equals "0" "$?" "Report does not include added cookie"

  ./verify-dast-schema.py output/report_test_browserker_scan.json
}
