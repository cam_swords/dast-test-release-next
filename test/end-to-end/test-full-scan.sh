#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null

  docker run --rm \
    -v "${PWD}/fixtures/webgoat-8.0.0.M21":/home/webgoat/.webgoat-8.0.0.M21 \
    --name goat \
    --network test \
    -d \
    registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e >/dev/null

  # start nginx container to use as proxy for domain validation
  docker run --rm \
    -v "${PWD}/fixtures/domain-validation/nginx.conf":/etc/nginx/conf.d/default.conf \
    --name vulnerabletestserver \
    --network test \
    -d \
    nginx:1.17.6-alpine >/dev/null
  true
}

teardown_suite() {
  docker rm -f goat vulnerabletestserver >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_webgoat_full_scan() {
  # /logout is excluded because it invalidates the session, causing many pages to not be spidered
  # css files are excluded to make the test run faster
  # bootstrap is removed because it causes an intermittent vulnerability result
  # Don't fuzz the Agent header (10104)
  # Don't check for suspicious comments (too many fuzzing options)
  docker run --rm \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED=true \
    --env DAST_EXCLUDE_RULES=10104,10027,20012 \
    --env DAST_PYTHON_MEMORY_PROFILE_REPORT=mprofile-test_webgoat_full_scan \
    -v "${PWD}":/output \
    -v "${PWD}/../../profiling":/output/profiling \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -j -t http://vulnerabletestserver/WebGoat/attack \
    --auth-url http://vulnerabletestserver/WebGoat/login \
    --auth-username "someone" \
    --auth-password "P@ssw0rd" \
    --auth-username-field "exampleInputEmail1" \
    --auth-password-field "exampleInputPassword1" \
    --auth-exclude-urls 'http://vulnerabletestserver/WebGoat/js/respond.min.js,http://vulnerabletestserver/.*.css,http://vulnerabletestserver/WebGoat/plugins/bootstrap/css/bootstrap.min.css' \
    --auth-verification-url 'http://vulnerabletestserver/WebGoat/start.mvc' \
    >output/test_webgoat_full_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_webgoat_full_scan.json

  # this cookie sometimes is populated, and sometimes is not. when it is populated, the value will be different each time
  # the fuzzer count is different each time, presumably because it is fuzzing
  # note sed -i is not used as it does not work consistently on different operating systems
  sed 's/The user-controlled value was:<\/p><p>[a-zA-Z0-9]*/The user-controlled value was:<\/p><p>__REMOVED__/g
       s/The user-controlled value was: [a-zA-Z0-9]*/The user-controlled value was: __REMOVED__/g
       s/JSESSIONID=[A-Z0-9]*/JSESSIONID=__REMOVED__/g
       s/matchingPassword=[a-zA-Z0-9]*/matchingPassword=__REMOVED__/g
       s/agree=[a-zA-Z0-9]*/matchingPassword=__REMOVED__/g
       s/password=[a-zA-Z0-9]*/matchingPassword=__REMOVED__/g
       s/tag \[[a-zA-Z]*\] attribute/tag [__REMOVED__] attribute/g
       s/\\"username\\"/__REMOVED__/g
       s/\\"password\\"/__REMOVED__/g
       s/\\"matchingPassword\\"/__REMOVED__/g
       s/\\"agree\\"/__REMOVED__/g
       s/\\"exampleInputEmail1\\"/__REMOVED__/g
       s/\\"exampleInputPassword1\\"/__REMOVED__/g
       s/__REMOVED__ __REMOVED__ __REMOVED__ __REMOVED__/__REMOVED__/g
       s/__REMOVED__ __REMOVED__ __REMOVED__/__REMOVED__/g
       s/__REMOVED__ __REMOVED__/__REMOVED__/g
       s/username=[a-zA-Z0-9]*/matchingPassword=__REMOVED__/g' gl-dast-report.json > temp.json && mv temp.json gl-dast-report.json

  diff -u <(./normalize_dast_report.py expect/test_webgoat_full_scan.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  grep -q 'The following [0-9]* URLs were scanned' output/test_webgoat_full_scan.log
  assert_equals "0" "$?" "Logged output missing scanned URLs count"
  grep -q 'Active scan rule Cookie Slack Detector \[90027\] \[Complete\] ran' output/test_webgoat_full_scan.log
  assert_equals "0" "$?" "Logged output missing active scan rule"

  grep -q 'SKIP: Anti-CSRF Tokens Check \[20012\]' output/test_webgoat_full_scan.log && \
  grep -q 'SKIP: User Agent Fuzzer \[10104\]' output/test_webgoat_full_scan.log && \
  grep -q 'SKIP: Information Disclosure - Suspicious Comments \[10027\]' output/test_webgoat_full_scan.log && \
  grep -q 'SKIP: Timestamp Disclosure \[10096\]' output/test_webgoat_full_scan.log
  assert_equals "0" "$?" "Rules were not skipped as intended"

  ./verify-dast-schema.py output/report_test_webgoat_full_scan.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}

test_webgoat_full_scan_domain_validation() {
  docker run --rm\
    --env DAST_FULL_SCAN_ENABLED=True \
    --env DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED=1 \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t http://goat:8080/WebGoat/attack \
    >output/test_webgoat_full_scan_domain_validation.log 2>&1

  assert_equals "1" "$?" "Expected DAST domain validation to fail but it did not"
}
