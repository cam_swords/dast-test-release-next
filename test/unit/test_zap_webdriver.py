import unittest
from unittest.mock import MagicMock, call

from src.zap_webdriver import ZapWebdriver


class TestZapWebdriver(unittest.TestCase):

    def setUp(self):
        self.webdriver = ZapWebdriver(MagicMock())
        self.webdriver.auth_username = 'user'
        self.webdriver.auth_password = 'pass'

    def test_auto_login_searches_and_fills_in_user(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("(//input[(@type='text' and contains(@name,'ser'))"
                              " or @type='text'])[1]"),
                         call().clear(),
                         call().send_keys(self.webdriver.auth_username)]
        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_auto_login_searches_and_fills_in_pass(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("//input[@type='password' or contains(@name,'ass')]"),
                         call().clear(),
                         call().send_keys(self.webdriver.auth_password)]
        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_auto_login_searches_and_clicks_submit(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("//*[@type='submit' or @type='button']"),
                         call().click()]

        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_cleanup_is_silent_when_webdriver_is_not_used(self):
        self.webdriver.driver = None

        # no error should be thrown
        self.webdriver.cleanup()

    def test_cleanup_releases_resources(self):
        self.webdriver.driver = MagicMock()

        self.webdriver.cleanup()

        self.webdriver.driver.quit.assert_called_once()
