from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, mock_open, patch

from src.scan_script_wrapper import ScanScriptWrapper
from src.zap_log_configuration import ZAPLogConfiguration
from test.unit.mock_config import ToConfig


@patch('src.scan_script_wrapper.System')
@patch('builtins.open', new_callable=mock_open)
class TestScanScriptWrapper(TestCase):

    def setUp(self):
        self.active_scan_policy = MagicMock()
        self.config = ToConfig(target='http://website')
        self.zap_log_configuration = ZAPLogConfiguration(self.config)
        self.site_check = MagicMock(is_available=MagicMock(return_value=True),
                                    unavailable_reason=MagicMock(return_value=None),
                                    is_safe_to_scan=MagicMock(return_value=(True, None)))
        self.zap_server = MagicMock()
        self.zaproxy = MagicMock()
        self.custom_hooks = MagicMock()

    def _run_scan_script_wrapper(self):
        ScanScriptWrapper(
            self.config,
            self.zap_log_configuration,
            self.active_scan_policy,
            self.zap_server,
            self.zaproxy,
            self.custom_hooks,
        ).run()

    def test_should_start_zap_server_and_configure_client_with_port(self, _, mock_system):
        site_check = MagicMock(
            is_available=MagicMock(return_value=True),
            unavailable_reason=MagicMock(return_value=None),
            is_safe_to_scan=MagicMock(return_value=(True, None)))

        self.target_website = MagicMock(
            address=MagicMock(return_value='http://website'),
            is_configured=MagicMock(return_value=True),
            is_url=MagicMock(return_value=True),
            check_site_is_available=MagicMock(return_value=site_check))

        zap_daemon = MagicMock()
        zap_daemon.proxy_endpoint.return_value = 'http://127.0.0.1:5001'

        self.zap_server.start.return_value = zap_daemon
        self.zaproxy.connect.return_value = 'zap_client'

        self._run_scan_script_wrapper()

        self.zaproxy.connect.assert_called_with('http://127.0.0.1:5001')
        self.custom_hooks.handover_to_dast.assert_called_once_with('zap_client', zap_daemon)

    def test_should_print_version(self, _, mock_system):
        site_check = MagicMock(
            is_available=MagicMock(return_value=True),
            unavailable_reason=MagicMock(return_value=None),
            is_safe_to_scan=MagicMock(return_value=(True, None)))

        self.target_website = MagicMock(
            address=MagicMock(return_value='http://website'),
            is_configured=MagicMock(return_value=True),
            is_url=MagicMock(return_value=True),
            check_site_is_available=MagicMock(return_value=site_check))

        with patch('src.scan_script_wrapper.logging') as logging:
            system = MagicMock()
            system.dast_version.return_value = '1.27.0'
            system.python_version.return_value = '3.8.2'
            mock_system.return_value = system
            self._run_scan_script_wrapper()

        self.assertIn('DAST 1.27.0', logging.info.mock_calls[0][1][0])
        self.assertIn('Python 3.8.2', logging.info.mock_calls[0][1][0])

    def test_should_continue_scanning_even_if_target_website_is_not_available(self, _, mock_system):
        site_check = MagicMock(
            is_available=MagicMock(return_value=False),
            unavailable_reason=MagicMock(return_value='a connect error'),
            is_safe_to_scan=MagicMock(return_value=(True, None)),
        )

        self.target_website = MagicMock(
            address=MagicMock(return_value='http://website'),
            is_configured=MagicMock(return_value=True),
            is_url=MagicMock(return_value=True),
            check_site_is_available=MagicMock(return_value=site_check),
        )

        with patch.multiple('src.scan_script_wrapper',
                            logging=DEFAULT,
                            System=DEFAULT) as mock_dependencies:
            system = MagicMock()
            mock_dependencies['System'].return_value = system
            self._run_scan_script_wrapper()

        zap_client = self.zaproxy.connect.return_value
        zap_daemon = self.zap_server.start.return_value

        system.sys_exit.assert_not_called()
        self.custom_hooks.handover_to_dast.assert_called_once_with(zap_client, zap_daemon)

    def test_opens_the_zap_log_config_file(self, mock, mock_system):
        self._run_scan_script_wrapper()
        mock.assert_called_once_with('/app/zap/log4j.properties', 'w+')

    def test_writes_the_zap_log_config_file(self, mock, mock_system):
        self._run_scan_script_wrapper()
        self.assertIn('log4j.rootLogger=INFO', mock().__enter__().write.call_args[0][0])

    def test_writes_an_active_scan_policy(self, _mock_open, _mock_system):
        self.config = ToConfig(target='http://website', full_scan=True, exclude_rules=['10005'])
        self._run_scan_script_wrapper()

        self.assertEqual(1, len(self.active_scan_policy.write_policy.mock_calls))

    def test_does_not_write_an_active_scan_policy_if_no_rules_are_excluded(self, _mock_open, mock_system):
        self.config = ToConfig(target='http://website', full_scan=True, exclude_rules=[])
        self._run_scan_script_wrapper()

        self.assertEqual(0, len(self.active_scan_policy.write_policy.mock_calls))

    def test_does_not_write_an_active_scan_policy_if_not_a_full_scan(self, _mock_open, mock_system):
        self.config = ToConfig(target='http://website', full_scan=False, exclude_rules=['1005'])
        self._run_scan_script_wrapper()

        self.assertEqual(0, len(self.active_scan_policy.write_policy.mock_calls))
