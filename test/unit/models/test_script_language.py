from unittest import TestCase

from src.models.script_language import ScriptLanguage


class TestScriptLanguage(TestCase):

    def test_should_detect_language_for_supported_file(self):
        language = ScriptLanguage.detect_language('/test/script.js')
        self.assertEqual(ScriptLanguage.JAVASCRIPT, language)

    def test_should_not_detect_language_when_extension_not_supported(self):
        self.assertIsNone(ScriptLanguage.detect_language('/test/script.txt'))

    def test_should_not_detect_language_when_file_path_not_valid(self):
        self.assertIsNone(ScriptLanguage.detect_language(''))
