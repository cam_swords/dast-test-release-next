from unittest import TestCase

from src.models import WascID, WebApplicationSecurityConsortium


class TestWascID(TestCase):

    def test_valid_returns_true_if_id_is_non_zero(self):
        wasc_id = WascID('1')

        self.assertTrue(wasc_id.valid())

    def test_valid_returns_false_if_id_is_zero(self):
        wasc_id = WascID('0')

        self.assertFalse(wasc_id.valid())

    def test_cast_to_int_throws_accurate_error(self):
        wasc_id = WascID('0')

        with self.assertRaises(ValueError) as error:
            int(wasc_id)

        self.assertEqual(str(error.exception), 'ID must be a non-zero integer')


class TestWebApplicationSecurityConsortium(TestCase):

    def test_should_return_wasc_reference(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id(WascID('16'))
        self.assertIn('Directory-Indexing', url)

    def test_should_return_wasc_reference_for_single_digit(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id(WascID('1'))
        self.assertIn('Insufficient-Authentication', url)

    def test_should_return_wasc_reference_for_single_digit_with_trailing_zero(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id(WascID('01'))
        self.assertIn('Insufficient-Authentication', url)

    def test_should_return_overview_page_when_not_found(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id(WascID('666'))
        self.assertEqual(WebApplicationSecurityConsortium.OVERVIEW_REFERENCE, url)

    def test_should_throw_up_when_id_is_not_present(self):
        try:
            WebApplicationSecurityConsortium().reference_url_for_id(WascID(''))
            self.assertTrue(False)
        except ValueError as e:
            self.assertIn('unable to determine reference url', str(e))

    def test_should_throw_up_when_id_is_not_a_number(self):
        try:
            WebApplicationSecurityConsortium().reference_url_for_id(WascID('sixsixsix'))
            self.assertTrue(False)
        except ValueError as e:
            self.assertIn('unable to determine reference url', str(e))
