from unittest import TestCase

from src.models import Target


class TestTarget(TestCase):

    def test_is_host_url_return_false_if_target_has_more_than_two_slashes(self):
        target = Target('http://target.test/some_path')

        self.assertFalse(target.is_host_url())

    def test_is_host_url_return_false_if_target_has_less_than_two_slashes(self):
        target = Target('http://target.test')

        self.assertTrue(target.is_host_url())

    def test_reset_to_host_removes_path_from_host_url(self):
        target = Target('http://target.test/some_path')

        self.assertEqual(target.reset_to_host(), 'http://target.test')

    def test_build_url_combines_the_target_and_path(self):
        target = Target('http://target.test')

        self.assertEqual(target.build_url('/path/1.html'), 'http://target.test/path/1.html')

    def test_build_url_strips_unnecessary_white_space(self):
        target = Target('http://target.test')

        self.assertEqual(target.build_url('  /path/1.html  '), 'http://target.test/path/1.html')

    def test_can_parse_hostname(self):
        self.assertEqual(Target('http://my.site.com').hostname(), 'my.site.com')
        self.assertEqual(Target('https://my.site.com').hostname(), 'my.site.com')
        self.assertEqual(Target('https://my.site.com:8080').hostname(), 'my.site.com')
        self.assertEqual(Target('https://site.com:8080/section/page?sort=true').hostname(), 'site.com')

    def test_when_url_is_none_target_should_be_None(self):
        self.assertFalse(Target(None))
