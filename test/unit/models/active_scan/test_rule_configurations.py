from unittest import TestCase

from src.models.active_scan.rule_configurations import RuleConfigurations
from test.unit import factories


class TestRuleConfigurations(TestCase):

    def test_should_reduce_rules(self):
        rule_configurations = RuleConfigurations([
            factories.models.active_scan.rule_configuration(rule_id='1005'),
            factories.models.active_scan.rule_configuration(rule_id='1006'),
            factories.models.active_scan.rule_configuration(rule_id='1007'),
        ])

        reduced = rule_configurations.reduce('!'.join, lambda rc: rc.rule_id)
        self.assertEqual('1005!1006!1007', reduced)

    def test_should_concat_rule_configurations(self):
        rc_a = RuleConfigurations([factories.models.active_scan.rule_configuration(rule_id='1005')])
        rc_b = RuleConfigurations([factories.models.active_scan.rule_configuration(rule_id='1006')])

        rc_c = rc_a.concat(rc_b)

        self.assertEqual(2, len(rc_c.rules))
        self.assertEqual('1005', rc_c.rules[0].rule_id)
        self.assertEqual('1006', rc_c.rules[1].rule_id)
