from unittest import TestCase

from test.unit.factories.models.active_scan import scan_policy


class TestScanPolicy(TestCase):

    def test_name_returns_file_name_without_extension(self):
        policy = scan_policy(file_name='test-policy.policy')

        self.assertEqual(policy.name(), 'test-policy')
