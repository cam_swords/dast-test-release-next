from src.http_headers import HttpHeaders


def message(method='GET',
            request_headers=HttpHeaders(),
            url='http://website.com/',
            status_code=200,
            reason_phrase='OK',
            response_headers=HttpHeaders()):
    return {
        '_zapMessageId': '1',
        '_zapMessageNote': '',
        '_zapMessageType': '1',
        'cache': {},
        'request': {'bodySize': 0,
                    'cookies': [],
                    'headers': request_headers,
                    'headersSize': 116,
                    'httpVersion': 'HTTP/1.1',
                    'method': method,
                    'postData': {'mimeType': '', 'params': [], 'text': ''},
                    'queryString': [],
                    'url': url},
        'response': {'bodySize': 277,
                     'content': {'compression': 0,
                                 'mimeType': 'text/html',
                                 'size': 277,
                                 'text': '<!DOCTYPE html>\n'
                                         '<html>\n'
                                         '<head>\n'
                                         '<title>Example</title>\n'
                                         '</head>\n'
                                         '<body>\n'
                                         '<form action="/myform" method="POST">\n'
                                         '<input type="text" name="name" value="You name"/>\n'
                                         '<input type="submit" value="Tell me your name"/>\n'
                                         '</form>\n'
                                         '</body>\n'
                                         '</html>\n'},
                     'cookies': [],
                     'headers': response_headers,
                     'headersSize': 238,
                     'httpVersion': 'HTTP/1.1',
                     'redirectURL': '',
                     'status': status_code,
                     'statusText': reason_phrase},
        'startedDateTime': '2020-05-05T06:39:43.835+00:00',
        'time': 30,
        'timings': {'receive': 30, 'send': 0, 'wait': 0},
    }
