from ..models.http import http_message as http_message_builder
from ..zap_api import alert as api_alert


def alert(other='The X-XSS-Protection HTTP response header allows the web server ...',
          plugin_id='10016',
          wasc_id='14',
          cwe_id='933',
          confidence='Medium',
          risk='Low',
          url='http://192.168.1.105/',
          evidence='4111111111111111',
          name='Content Security Policy (CSP) Header Not Set',
          source_id='3',
          method='GET',
          param='test.param',
          attack='test.attack',
          alert_id='10',
          reference='https://developer.mozilla.org/en-US/docs/Web/Security/CSP/Introducing_Content_Security_Policy\n'
                    'https://www.owasp.org/index.php/Content_Security_Policy\n',
          solution='Ensure that your web server is configured to set the Content-Security-Policy header',
          description='Content Security Policy (CSP) is an added layer of security',
          message=http_message_builder()):
    content = api_alert(other=other,
                        plugin_id=plugin_id,
                        wasc_id=wasc_id,
                        cwe_id=cwe_id,
                        confidence=confidence,
                        risk=risk,
                        url=url,
                        evidence=evidence,
                        name=name,
                        sourceid=source_id,
                        method=method,
                        param=param,
                        attack=attack,
                        alert_id=alert_id,
                        reference=reference,
                        solution=solution,
                        description=description)
    content['message'] = message

    return content
