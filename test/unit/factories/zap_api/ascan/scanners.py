from typing import Dict, List


def scanners(*scnrs) -> List[Dict[str, str]]:
    if not scnrs:
        return [scanner()]

    return list(scnrs)


def scanner(plugin_id='100095',
            enabled='true',
            all_dependencies_available='true',
            policy_id='0',
            cwe_id='530',
            attack_strength='DEFAULT',
            alert_threshold='DEFAULT',
            name='Backup File Disclosure',
            wasc_id='34',
            quality='beta',
            dependencies=None) -> Dict[str, str]:
    return {'allDependenciesAvailable': all_dependencies_available,
            'policyId': policy_id,
            'cweId': cwe_id,
            'attackStrength': attack_strength,
            'alertThreshold': alert_threshold,
            'name': name,
            'wascId': wasc_id,
            'id': plugin_id,
            'enabled': enabled,
            'quality': quality,
            'dependencies': dependencies if dependencies else []}
