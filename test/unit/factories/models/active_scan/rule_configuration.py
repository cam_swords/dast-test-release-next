from src.models.active_scan.alert_threshold import AlertThreshold
from src.models.active_scan.rule_configuration import RuleConfiguration


def rule_configuration(rule_id: str = '10001',
                       enabled: bool = True,
                       level: AlertThreshold = AlertThreshold.MEDIUM) -> RuleConfiguration:
    return RuleConfiguration(rule_id, enabled, level)
