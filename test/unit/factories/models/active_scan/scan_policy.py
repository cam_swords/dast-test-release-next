from src.models.active_scan.alert_threshold import AlertThreshold
from src.models.active_scan.rule_configurations import RuleConfigurations
from src.models.active_scan.rule_strength import RuleStrength
from src.models.active_scan.scan_policy import ScanPolicy


def scan_policy(file_name: str = 'stub-policy.policy',
                level: AlertThreshold = AlertThreshold.MEDIUM,
                strength: RuleStrength = RuleStrength.HIGH,
                rule_configurations: RuleConfigurations = RuleConfigurations()):

    return ScanPolicy(file_name=file_name,
                      level=level,
                      strength=strength,
                      rule_configurations=rule_configurations)
