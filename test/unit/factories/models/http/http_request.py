from src.http_headers import HttpHeaders
from src.models.http import HttpRequest


def http_request(method: str = 'GET',
                 url: str = 'http://site',
                 headers: HttpHeaders = None) -> HttpRequest:
    http_headers = headers if headers else HttpHeaders()

    return HttpRequest(method, url, http_headers)
