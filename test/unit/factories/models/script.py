from src.models import Script, ScriptType


def script(script_type: ScriptType = ScriptType.HTTP_SENDER,
           name: str = 'script.name',
           file_path: str = '/tmp/script.js'):
    return Script(script_type, name, file_path)
