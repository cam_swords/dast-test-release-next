import unittest
from datetime import datetime, timezone
from unittest.mock import MagicMock

from src.models import CweID, RuleID, SourceID, WascID
from src.report import ModifiedZapReportFormatter
from test.unit.factories.models import f_alert, f_alerts


def zap_alerts():
    return f_alerts(
        f_alert(
            source_id='3',
            other='',
            method='GET',
            evidence='',
            plugin_id='10020',
            cwe_id='16',
            confidence='Medium',
            wasc_id='15',
            description='X-Frame-Options header is not included in the HTTP response...',
            url='http://nginx/b.location',
            reference='http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...',
            solution='Most modern Web browsers support the X-Frame-Options HTTP header...',
            param='X-Frame-Options',
            attack='',
            name='X-Frame-Options Header Not Set',
            risk='Medium',
            alert_id='2',
        ),
        f_alert(
            source_id='3',
            other='The X-XSS-Protection HTTP response header allows the web server ...',
            method='POST',
            evidence='<form action="/myform" method="POST">',
            plugin_id='10016',
            cwe_id='933',
            confidence='Medium',
            wasc_id='14',
            description='Web Browser XSS Protection is not enabled, or is disabled...',
            url='http://nginx/',
            reference='http://blogs.msdn.com/a\nhttp://blogs.msdn.com/b',
            solution='Ensure that the web browsers XSS filter is enabled...',
            param='X-XSS-Protection',
            attack='http://nginx/attack',
            name='Web Browser XSS Protection Not Enabled',
            risk='Low',
            alert_id='1',
        ),
        f_alert(
            source_id='3',
            other='',
            method='GET',
            evidence='',
            plugin_id='10020',
            cwe_id='16',
            confidence='Medium',
            wasc_id='15',
            description='X-Frame-Options header is not included in the HTTP response...',
            url='http://nginx/a.location',
            reference='http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...',
            solution='Most modern Web browsers support the X-Frame-Options HTTP header...',
            param='X-Frame-Options',
            attack='',
            name='X-Frame-Options Header Not Set',
            risk='Medium',
            alert_id='2',
        ),
    )


def spider_scan():
    return {
        'progress': '100',
        'state': 'FINISHED',
        'id': '0',
    }


def spider_scan_result():
    return [
        {
            'urlsInScope': [
                {
                    'url': 'http://f.url',
                    'statusReason': 'OK',
                    'reasonNotProcessed': '',
                    'processed': 'true',
                    'method': 'GET',
                    'statusCode': '200',
                    'messageId': '1',
                },
                {
                    'url': 'http://e.url',
                    'statusReason': 'OK',
                    'reasonNotProcessed': '',
                    'processed': 'true',
                    'method': 'GET',
                    'statusCode': '201',
                    'messageId': '2',
                },
            ],
        },
        {
            'urlsOutOfScope': ['http://d.url', 'http://c.url'],
        },
        {
            'urlsIoError': [
                {
                    'url': 'http://b.url',
                    'statusReason': 'Internal Server Error',
                    'reasonNotProcessed': '',
                    'processed': 'false',
                    'method': 'GET',
                    'statusCode': '500',
                },
                {
                    'url': 'http://a.url',
                    'statusReason': 'Bad Gateway',
                    'reasonNotProcessed': '',
                    'processed': 'false',
                    'method': 'GET',
                    'statusCode': '502',
                },
            ],
        },
    ]


class TestModifiedZapReportFormatter(unittest.TestCase):

    def setUp(self):
        alerts = zap_alerts()
        self.zap_version = 'D-2019-09-23'
        self.system = MagicMock(date_time=MagicMock(return_value=datetime.today()))
        self.formatter = ModifiedZapReportFormatter(self.system)
        self.report = self.formatter.format_report(
            self.zap_version,
            alerts,
            '',
            None,
            spider_scan(),
            spider_scan_result(),
        )

    def test_should_output_generated_time(self):
        utc = timezone.utc
        today = datetime(2019, 12, 6, 16, 29, 43, 50218, utc)

        formatter = ModifiedZapReportFormatter(MagicMock(current_date_time=MagicMock(return_value=today)))
        self.report = formatter.format_report(
            self.zap_version,
            zap_alerts(),
            '',
            None,
            spider_scan(),
            spider_scan_result(),
        )

        self.assertEqual(self.report['@generated'], 'Fri, 06 Dec 2019 16:29:43')

    def test_should_remove_message_ids(self):
        self.assertEqual(len(self.report['spider']['result']['urlsInScope']), 2)
        self.assertFalse('messageId' in self.report['spider']['result']['urlsInScope'][0])
        self.assertFalse('messageId' in self.report['spider']['result']['urlsInScope'][1])

    def test_should_remove_scan_id(self):
        self.assertFalse('scanId' in self.report['spider'])

    def test_urls_should_be_ordered_by_url_and_method(self):
        self.assertEqual(len(self.report['spider']['result']['urlsInScope']), 2)
        self.assertEqual(self.report['spider']['result']['urlsInScope'][0]['url'], 'http://e.url')
        self.assertEqual(self.report['spider']['result']['urlsInScope'][1]['url'], 'http://f.url')

        self.assertEqual(len(self.report['spider']['result']['urlsOutOfScope']), 2)
        self.assertEqual(self.report['spider']['result']['urlsOutOfScope'][0], 'http://c.url')
        self.assertEqual(self.report['spider']['result']['urlsOutOfScope'][1], 'http://d.url')

        self.assertEqual(len(self.report['spider']['result']['urlsIoError']), 2)
        self.assertEqual(self.report['spider']['result']['urlsIoError'][0]['url'], 'http://a.url')
        self.assertEqual(self.report['spider']['result']['urlsIoError'][1]['url'], 'http://b.url')

    def test_url_keys_should_be_ordered_alphabetically(self):
        keys = list(self.report['spider']['result'].keys())
        self.assertEqual(keys[0], 'urlsInScope')
        self.assertEqual(keys[1], 'urlsIoError')
        self.assertEqual(keys[2], 'urlsOutOfScope')

        first_in_scope_url = list(self.report['spider']['result']['urlsInScope'][0].keys())
        self.assertEqual(first_in_scope_url[0], 'method')
        self.assertEqual(first_in_scope_url[1], 'processed')
        self.assertEqual(first_in_scope_url[2], 'reasonNotProcessed')
        self.assertEqual(first_in_scope_url[3], 'statusCode')
        self.assertEqual(first_in_scope_url[4], 'statusReason')
        self.assertEqual(first_in_scope_url[5], 'url')

    def test_should_collect_all_scanned_result(self):
        scan_results = [{'urlsOutOfScope': ['http://url.2']},
                        {'urlsOutOfScope': ['http://url.1']}]

        report = self.formatter.format_report(self.zap_version, zap_alerts(), '', None, spider_scan(), scan_results)

        self.assertEqual(len(report['spider']['result']['urlsInScope']), 0)
        self.assertEqual(len(report['spider']['result']['urlsOutOfScope']), 2)
        self.assertEqual(report['spider']['result']['urlsOutOfScope'][0], 'http://url.1')
        self.assertEqual(report['spider']['result']['urlsOutOfScope'][1], 'http://url.2')
        self.assertEqual(len(report['spider']['result']['urlsIoError']), 0)

    def test_should_include_spider_progress(self):
        self.assertEqual(self.report['spider']['state'], 'FINISHED')
        self.assertEqual(self.report['spider']['progress'], '100')

    def test_should_include_timestamp_and_version(self):
        self.assertEqual(self.report['@version'], 'D-2019-09-23')

    def test_should_include_site_metadata_http(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(self.report['site'][0]['@host'], 'nginx')
        self.assertEqual(self.report['site'][0]['@name'], 'http://nginx')
        self.assertEqual(self.report['site'][0]['@port'], '80')
        self.assertEqual(self.report['site'][0]['@ssl'], 'false')

    def test_should_include_site_metadata_https(self):
        alerts = [f_alert(url='https://my.site/page')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@host'], 'my.site')
        self.assertEqual(report['site'][0]['@name'], 'https://my.site')
        self.assertEqual(report['site'][0]['@port'], '443')
        self.assertEqual(report['site'][0]['@ssl'], 'true')

    def test_should_include_site_metadata_custom_port(self):
        alerts = [f_alert(url='https://my.site:558')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@host'], 'my.site')
        self.assertEqual(report['site'][0]['@name'], 'https://my.site:558')
        self.assertEqual(report['site'][0]['@port'], '558')

    def test_site_alerts_include_all_properties(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][1]
        self.assertEqual(alert['alert'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(alert['confidence'], '2')
        self.assertEqual(alert['count'], '1')
        self.assertEqual(alert['cweid'], CweID('933'))
        self.assertEqual(alert['desc'], '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>')
        self.assertEqual(alert['name'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(alert['otherinfo'],
                         '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>')
        self.assertEqual(alert['pluginid'], RuleID('10016'))
        self.assertEqual(alert['riskcode'], '1')
        self.assertEqual(alert['riskdesc'], 'Low (Medium)')
        self.assertEqual(alert['reference'], '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>')
        self.assertEqual(alert['solution'], '<p>Ensure that the web browsers XSS filter is enabled...</p>')
        self.assertEqual(alert['sourceid'], SourceID('3'))
        self.assertEqual(alert['wascid'], WascID('14'))

    def test_site_alerts_are_ordered_by_risk(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)
        self.assertEqual(self.report['site'][0]['alerts'][0]['name'], 'X-Frame-Options Header Not Set')
        self.assertEqual(self.report['site'][0]['alerts'][1]['name'], 'Web Browser XSS Protection Not Enabled')

    def test_site_alerts_include_instances(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][1]
        self.assertEqual(len(alert['instances']), 1)
        self.assertEqual(alert['instances'][0]['attack'], 'http://nginx/attack')
        self.assertEqual(alert['instances'][0]['evidence'], '<form action="/myform" method="POST">')
        self.assertEqual(alert['instances'][0]['method'], 'POST')
        self.assertEqual(alert['instances'][0]['param'], 'X-XSS-Protection')
        self.assertEqual(alert['instances'][0]['uri'], 'http://nginx/')

    def test_site_alerts_instances_are_sorted_by_uri(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][0]
        self.assertEqual(len(alert['instances']), 2)
        self.assertEqual(alert['instances'][0]['uri'], 'http://nginx/a.location')
        self.assertEqual(alert['instances'][1]['uri'], 'http://nginx/b.location')

    def test_site_alerts_otherinfo_is_empty_when_no_content(self):
        alerts = [f_alert(other='')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['otherinfo'], '')

    def test_site_alerts_otherinfo_converts_new_lines_with_carriage_return_to_new_html(self):
        alerts = [f_alert(other='Hello\r\nWorld')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['otherinfo'], '<p>Hello</p><p>World</p>')

    def test_site_alerts_solution_is_empty_html_when_no_content(self):
        alerts = [f_alert(solution='')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['solution'], '<p></p>')

    def test_should_barf_when_vulnerability_has_no_pluginid(self):
        alert = f_alert(alert_id='10', plugin_id='', name='Web Browser XSS Protection Not Enabled')

        with self.assertRaises(RuntimeError) as error:
            self.formatter.format_report(self.zap_version, [alert], '', None, spider_scan(), spider_scan_result())

        self.assertEqual(str(error.exception), "Unable to create DAST report as there is no pluginid for alert '10'")

    def test_should_support_multiple_sites(self):
        alerts = [
            f_alert(url='http://host.a/path', name='vulnerability.a'),
            f_alert(url='http://host.b/path', name='vulnerability.b'),
        ]

        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 2)

    def test_different_ports_should_have_different_sites(self):
        alerts = [
            f_alert(url='http://host.a/path', name='vulnerability.a'),
            f_alert(url='http://host.b/path', name='vulnerability.b'),
        ]

        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 2)

    def test_http_sites_with_implicit_port_should_be_grouped(self):
        alerts = [
            f_alert(url='http://host.com:80', name='vulnerability.a'),
            f_alert(url='http://host.com', name='vulnerability.b'),
        ]

        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@name'], 'http://host.com')
        self.assertEqual(report['site'][0]['@port'], '80')

    def test_https_sites_with_implicit_port_should_be_grouped(self):
        alerts = [
            f_alert(url='https://host.com:443', name='vulnerability.a'),
            f_alert(url='https://host.com', name='vulnerability.b'),
        ]

        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@name'], 'https://host.com')
        self.assertEqual(report['site'][0]['@port'], '443')

    def test_http_sites_with_different_ports_should_not_be_grouped(self):
        alerts = [
            f_alert(url='http://host.com:112', name='vulnerability.a'),
            f_alert(url='http://host.com:556', name='vulnerability.b'),
        ]

        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 2)
        self.assertEqual(report['site'][0]['@name'], 'http://host.com:112')
        self.assertEqual(report['site'][0]['@port'], '112')
        self.assertEqual(report['site'][1]['@name'], 'http://host.com:556')
        self.assertEqual(report['site'][1]['@port'], '556')

    def test_should_have_empty_cwe_when_zero_value(self):
        alerts = [f_alert(cwe_id='0')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['cweid'], CweID(''))

    def test_should_have_empty_wascid_when_zero_value(self):
        alerts = [f_alert(wasc_id='0')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['wascid'], WascID(''))

    def test_should_have_informational_riskcode(self):
        alerts = [f_alert(risk='Informational', confidence='High')]
        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['riskcode'], '0')
        self.assertEqual(report['site'][0]['alerts'][0]['confidence'], '3')
        self.assertEqual(report['site'][0]['alerts'][0]['riskdesc'], 'Informational (High)')

    def test_alerts_should_be_grouped_by_plugin_id_and_risk(self):
        alerts = [
            f_alert(plugin_id='10001', risk='Low', url='http://site.com/a'),
            f_alert(plugin_id='10001', risk='Low', url='http://site.com/b'),
            f_alert(plugin_id='10001', risk='Informational', url='http://site.com/c'),
            f_alert(plugin_id='10002', risk='Informational', url='http://site.com/d'),
        ]

        report = self.formatter.format_report(self.zap_version, alerts, '', None, spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 3)

        self.assertEqual(report['site'][0]['alerts'][0]['pluginid'], RuleID('10001'))
        self.assertEqual(len(report['site'][0]['alerts'][0]['instances']), 2)
        self.assertEqual(report['site'][0]['alerts'][0]['instances'][0]['uri'], 'http://site.com/a')
        self.assertEqual(report['site'][0]['alerts'][0]['instances'][1]['uri'], 'http://site.com/b')

        self.assertEqual(report['site'][0]['alerts'][1]['pluginid'], RuleID('10001'))
        self.assertEqual(len(report['site'][0]['alerts'][1]['instances']), 1)
        self.assertEqual(report['site'][0]['alerts'][1]['instances'][0]['uri'], 'http://site.com/c')

        self.assertEqual(report['site'][0]['alerts'][2]['pluginid'], RuleID('10002'))
        self.assertEqual(len(report['site'][0]['alerts'][2]['instances']), 1)
        self.assertEqual(report['site'][0]['alerts'][2]['instances'][0]['uri'], 'http://site.com/d')
