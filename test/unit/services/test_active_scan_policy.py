from io import StringIO
from unittest import TestCase
from unittest.mock import Mock

from src.models.active_scan.alert_threshold import AlertThreshold
from src.models.active_scan.rule_configurations import RuleConfigurations
from src.models.active_scan.rule_strength import RuleStrength
from src.services.active_scan_policy import ActiveScanPolicy
from test.unit import factories
from test.unit.mock_config import ToConfig


class TestActiveScanPolicy(TestCase):

    def test_api_scan_returns_path_to_minimal_policy(self):
        config = ToConfig(is_api_scan=True)
        default_scan_policy = factories.models.active_scan.scan_policy(file_name='Default Policy.policy')
        api_scan_policy = factories.models.active_scan.scan_policy(file_name='API-Minimal.policy')
        path = ActiveScanPolicy(config, default_scan_policy, api_scan_policy).get_path()

        self.assertEqual('/app/zap/policies/API-Minimal.policy', path)

    def test_normal_scan_returns_path_to_default_policy(self):
        config = ToConfig(is_api_scan=False)
        default_scan_policy = factories.models.active_scan.scan_policy(file_name='Default Policy.policy')
        api_scan_policy = factories.models.active_scan.scan_policy(file_name='API-Minimal.policy')
        path = ActiveScanPolicy(config, default_scan_policy, api_scan_policy).get_path()

        self.assertEqual('/app/zap/policies/Default Policy.policy', path)

    def test_should_write_policy(self):
        config = ToConfig(is_api_scan=False, exclude_rules=[])
        rules_configurations = RuleConfigurations([
            factories.models.active_scan.rule_configuration(rule_id='1005', enabled=True, level=AlertThreshold.HIGH),
            factories.models.active_scan.rule_configuration(rule_id='1002', enabled=False, level=AlertThreshold.OFF),
        ])
        default_scan_policy = factories.models.active_scan.scan_policy(file_name='scan-policy.policy',
                                                                       level=AlertThreshold.HIGH,
                                                                       strength=RuleStrength.MEDIUM,
                                                                       rule_configurations=rules_configurations)
        active_scan_policy = ActiveScanPolicy(config, default_scan_policy, factories.models.active_scan.scan_policy())

        file = StringIO()
        active_scan_policy.write_policy(file)

        self.assertIn('<policy>scan-policy</policy>', file.getvalue())
        self.assertIn('<level>HIGH</level>', file.getvalue())
        self.assertIn('<strength>MEDIUM</strength>', file.getvalue())
        self.assertIn('<p1005><enabled>true</enabled><level>HIGH</level></p1005>', file.getvalue())
        self.assertIn('<p1002><enabled>false</enabled><level>OFF</level></p1002>', file.getvalue())

    def test_should_write_policy_with_excluded_rules(self):
        config = ToConfig(is_api_scan=False, exclude_rules=['1007', '1008'])
        default_scan_policy = factories.models.active_scan.scan_policy(rule_configurations=RuleConfigurations())
        active_scan_policy = ActiveScanPolicy(config, default_scan_policy, factories.models.active_scan.scan_policy())

        file = StringIO()
        active_scan_policy.write_policy(file)

        self.assertIn('<p1007><enabled>false</enabled><level>OFF</level></p1007>', file.getvalue())
        self.assertIn('<p1008><enabled>false</enabled><level>OFF</level></p1008>', file.getvalue())

    def test_should_write_policy_when_there_are_no_rules(self):
        config = ToConfig(is_api_scan=False, exclude_rules=[])
        default_scan_policy = factories.models.active_scan.scan_policy(file_name='empty-scan-policy.policy')
        active_scan_policy = ActiveScanPolicy(config, default_scan_policy, factories.models.active_scan.scan_policy())

        file = StringIO()
        active_scan_policy.write_policy(file)

        self.assertIn('<policy>empty-scan-policy</policy>', file.getvalue())

    def test_active_policy_returns_api_policy_if_api_scan(self):
        config = ToConfig(is_api_scan=True)
        default_scan_policy = Mock('Default Policy')
        api_scan_policy = Mock('API-Minimal')
        active_scan_policy = ActiveScanPolicy(config, default_scan_policy, api_scan_policy)

        active_policy = active_scan_policy.active_policy()

        self.assertEqual(active_policy, api_scan_policy)

    def test_active_policy_returns_default_policy_if_not_api_scan(self):
        config = ToConfig(is_api_scan=False)
        default_scan_policy = Mock('Default Policy')
        api_scan_policy = Mock('API-Minimal')
        active_scan_policy = ActiveScanPolicy(config, default_scan_policy, api_scan_policy)

        active_policy = active_scan_policy.active_policy()

        self.assertEqual(active_policy, default_scan_policy)
