from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, patch

from src.models.target import Target
from src.services.browserker import BrowserkerScan
from test.unit.mock_config import ToConfig


class TestBrowserkerScan(TestCase):

    def setUp(self) -> None:
        self.config = ToConfig(browserker_allowed_hosts=[])
        self.target = Target('http://example.com/path/1')

    def test_starts_browserker_process(self):
        with patch.multiple(
                'src.services.browserker.browserker_scan',
                System=DEFAULT,
                BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            mock_dependencies['System'].return_value = system
            system.run.return_value = process
            BrowserkerScan(self.config, self.target, MagicMock()).run()

        mock_dependencies['BrowserkerConfigurationFile'].return_value.write.assert_called_once()
        system.run.assert_called_once()
        process.poll.assert_called_once()

        self.assertIn('/browserker/analyzer', system.run.mock_calls[0][1][0])
        self.assertIn('run', system.run.mock_calls[0][1][0])

    def test_sets_global_exclude_urls(self):
        self.config.browserker_allowed_hosts = ['foo.com', 'bar.org', 'example.com']
        with patch.multiple(
                'src.services.browserker.browserker_scan',
                System=DEFAULT,
                BrowserkerConfigurationFile=DEFAULT):
            zaproxy = MagicMock()
            BrowserkerScan(self.config, self.target, zaproxy).run()

        zaproxy.set_global_exclude_urls.assert_called_once_with(
            '^(?:(?!https?:\\/\\/(foo.com|bar.org|example.com)).*).$')
