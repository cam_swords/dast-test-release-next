from unittest import TestCase
from unittest.mock import mock_open, patch

from src.services.browserker import BrowserkerConfigurationFile
from test.unit.mock_config import ToConfig


class TestBrowserkerConfigurationFile(TestCase):

    def test_writes_configuration_file(self):
        config = ToConfig(target='http://my.site.com:3000/context/page.html',
                          zap_port=9000,
                          exclude_urls=[],
                          browserker_allowed_hosts=['my.site.com'],
                          browserker_excluded_hosts=[],
                          browserker_ignored_hosts=[],
                          browserker_max_actions=20000,
                          browserker_max_attack_failures=2,
                          browserker_max_depth=4,
                          browserker_number_of_browsers=7)
        output = self._build_file(config)

        self.assertIn('AllowedHosts = ["my.site.com"]', output)
        self.assertIn('DataPath = "/output/browserker_data"', output)
        self.assertIn('DebugLogFile = "/output/browserker-debug.log"', output)
        self.assertIn('DebugLogLevel = "debug"', output)
        self.assertIn('DisableHeadless = false', output)
        self.assertIn('ExcludedHosts = []', output)
        self.assertIn('ExcludedURIs = []', output)
        self.assertIn('IgnoredHosts = []', output)
        self.assertIn('JSPluginPath = "/browserker/plugins/"', output)
        self.assertIn('LogLevel = "info"', output)
        self.assertIn('MaxActions = 20000', output)
        self.assertIn('MaxAttackFailures = 2', output)
        self.assertIn('MaxDepth = 4', output)
        self.assertIn('NumBrowsers = 7', output)
        self.assertIn('PassiveMode = true', output)
        self.assertIn('PluginResourcePath = "/browserker/resources/"', output)
        self.assertIn('Proxy = "http://127.0.0.1:9000"', output)
        self.assertIn('URL = "http://my.site.com:3000/context/page.html"', output)

    def test_configures_excluded_urls(self):
        config = ToConfig(exclude_urls=['http://my.site/a', 'http://my.site/b/.*'])
        output = self._build_file(config)

        self.assertIn('ExcludedURIs = ["http://my.site/a", "http://my.site/b/.*"]', output)
        self.assertNotIn('ExcludedURIs = []', output)

    def test_configures_excluded_hosts(self):
        config = ToConfig(browserker_excluded_hosts=['site-a.com', '', 'site-b.com'])
        output = self._build_file(config)

        self.assertIn('ExcludedHosts = ["site-a.com", "site-b.com"]', output)

    def test_configures_allowed_hosts(self):
        config = ToConfig(browserker_allowed_hosts=['my.site.com', 'another-site.com'])
        output = self._build_file(config)

        self.assertIn('AllowedHosts = ["my.site.com", "another-site.com"]', output)

    def test_configures_ignored_hosts(self):
        config = ToConfig(browserker_ignored_hosts=['site-a.com', 'site-b.com'])
        output = self._build_file(config)

        self.assertIn('IgnoredHosts = ["site-a.com", "site-b.com"]', output)

    def test_doesnt_exclude_authentication_page(self):
        config = ToConfig(exclude_urls=['http://my.site/login-page', 'http://my.site/large-download'],
                          auth_url='http://my.site/login-page')
        output = self._build_file(config)

        self.assertIn('ExcludedURIs = ["http://my.site/large-download"]', output)

    def test_configures_form_authentication(self):
        config = ToConfig(auth_username='usr', auth_password='pwd', exclude_urls=[])
        output = self._build_file(config)

        self.assertIn('[FormData]', output)
        self.assertIn('UserName = "usr"', output)
        self.assertIn('Password = "pwd"', output)

    def test_configures_form_email_if_user_is_email_address(self):
        config = ToConfig(auth_username='usr@site.com', auth_password='pwd', exclude_urls=[])
        output = self._build_file(config)

        self.assertIn('[FormData]', output)
        self.assertIn('Email = "usr@site.com"', output)

    def test_configures_custom_request_headers(self):
        config = ToConfig(request_headers={'Accept': 'application/json', 'Cache-Control': 'no-cache'})
        output = self._build_file(config)

        self.assertIn('[CustomHeaders]', output)
        self.assertIn('Accept = "application/json"', output)
        self.assertIn('Cache-Control = "no-cache"', output)

    def test_configures_custom_cookies(self):
        config = ToConfig(browserker_cookies={'name': 'fred', 'sessionid': '123456789'})
        output = self._build_file(config)

        self.assertIn('[CustomCookies]', output)
        self.assertIn('name = "fred"', output)
        self.assertIn('sessionid = "123456789"', output)

    def _build_file(self, config) -> str:
        with patch('src.services.browserker.browserker_configuration_file.open', mock_open()) as handle_builder:
            BrowserkerConfigurationFile(config, '/tmp/config.toml').write()

        mock_handle = handle_builder()
        return ''.join([call[1][0] for call in mock_handle.write.mock_calls])
