from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, call, patch

from src.models import Target
from src.services import ActiveScan


def failing_start_active_scan(_target, _policy):
    raise ValueError


class TestActiveScan(TestCase):

    def setUp(self):
        self.scan_percent_complete = '50'
        self.target = Target('https://democracynow.org')
        self.zap = MagicMock()
        self.zap.active_scan_percent_complete = MagicMock(side_effect=self.mock_scan_percent_complete)
        self.zap.active_scan_progress = MagicMock(return_value='scan status details')
        self.zap.context_details.return_value = {'id': '666'}
        self.scan_policy = MagicMock()
        active_policy = MagicMock()
        active_policy.name = MagicMock(return_value='Default Policy')
        self.scan_policy.active_policy = MagicMock(return_value=active_policy)

    def test_run_starts_and_waits_for_active_scan(self):
        active_scan = ActiveScan(self.zap, self.target, self.scan_policy, True)

        with patch.multiple(
            'src.services.active_scan',
            ActiveScanLogger=DEFAULT,
            time=DEFAULT,
        ) as mock_dependencies:
            active_scan.run()

        mock_time = mock_dependencies['time']

        self.zap.run_active_scan.assert_called_once_with('https://democracynow.org', 'Default Policy', None)
        mock_time.sleep.assert_has_calls([call(5), call(5)])

    def test_run_uses_context_for_website_active_scan(self):
        active_scan = ActiveScan(self.zap, self.target, self.scan_policy, False)

        with patch.multiple(
            'src.services.active_scan',
            ActiveScanLogger=DEFAULT,
            time=DEFAULT,
        ):
            active_scan.run()

        self.zap.run_active_scan.assert_called_once_with('https://democracynow.org', 'Default Policy', '666')

    def test_run_logs_scan_progress(self):
        active_scan = ActiveScan(self.zap, self.target, self.scan_policy, False)

        with patch.multiple(
            'src.services.active_scan',
            ActiveScanLogger=DEFAULT,
            logging=DEFAULT,
            time=DEFAULT,
        ) as mock_dependencies:
            active_scan.run()

        mock_logging = mock_dependencies['logging']

        mock_logging.info.assert_has_calls([
            call('Active Scan https://democracynow.org with policy Default Policy'),
            call('Active scan context: 666'),
            call('Active Scan progress: 50%'),
            call('Active Scan complete'),
        ])

    def mock_scan_percent_complete(self, _scan_id):
        percent_complete = int(self.scan_percent_complete)

        self.scan_percent_complete = '100'

        return percent_complete

    def test_logs_rules(self):
        active_scan = ActiveScan(self.zap, self.target, self.scan_policy, True)

        with patch('src.services.active_scan.time'), \
             patch('src.services.active_scan.logging.info'), \
             patch('src.services.active_scan.ActiveScanLogger') as mock_logger:
            active_scan.run()

        self.zap.active_scan_progress.assert_called_once_with(self.zap.run_active_scan.return_value)
        mock_logger.assert_called_once_with(self.zap.active_scan_progress.return_value)
        mock_logger.return_value.log.assert_called_once()
