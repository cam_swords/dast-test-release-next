import datetime
from unittest import TestCase
from unittest.mock import DEFAULT, Mock, call, patch

from src.models.errors import TargetNotAccessibleError
from src.services import TargetAvailability


class TestTargetAvailability(TestCase):

    def setUp(self):
        self._utcnows = [
            datetime.datetime(2020, 10, 31, 0, 0, 4),
            datetime.datetime(2020, 10, 31, 0, 0, 2),
            datetime.datetime(2020, 10, 31, 0, 0, 2),
            datetime.datetime(2020, 10, 31, 0, 0, 0),
            datetime.datetime(2020, 10, 31, 0, 0, 0),
            datetime.datetime(2020, 10, 31, 0, 0, 0),
        ]

    def test_verify_pings_target(self):
        config = Mock()
        config.availability_timeout = 1
        mock_check = Mock()
        mock_check.is_safe_to_scan.return_value = (True, None)

        access = TargetAvailability('https://test.website', config)

        with patch('src.services.target_availability.TargetProbe') as mock_probe:
            mock_probe.return_value.send_ping.return_value = mock_check

            access.verify()

        mock_probe.assert_called_once_with('https://test.website', config)
        mock_probe.return_value.send_ping.assert_called_once()

    def test_verify_raises_error_if_domain_validation_fails(self):
        config = Mock()
        config.availability_timeout = 1
        mock_check = Mock()
        mock_check.is_safe_to_scan.return_value = (False, 'access denied')

        access = TargetAvailability('https://test.website', config)

        with self.assertRaises(TargetNotAccessibleError) as error:
            with patch('src.services.target_availability.TargetProbe') as mock_probe:
                mock_probe.return_value.send_ping.return_value = mock_check

                access.verify()

        self.assertIn('Domain validation failed due to: access denied', str(error.exception))

    def test_verify_pings_target_until_timeout(self):
        config = Mock()
        config.availability_timeout = 3
        mock_check = Mock()
        mock_check.unavailable_reason.return_value = 'website broken'
        mock_check.is_available.return_value = False
        mock_check.is_safe_to_scan.return_value = (True, None)

        access = TargetAvailability('https://test.website', config)

        with patch.multiple(
            'src.services.target_availability',
            TargetProbe=DEFAULT,
            datetime=DEFAULT,
            logging=DEFAULT,
            sleep=DEFAULT,
        ) as mock_deps:
            mock_datetime = mock_deps['datetime']
            mock_probe = mock_deps['TargetProbe']
            mock_datetime.utcnow.side_effect = self._now
            mock_probe.return_value.send_ping.return_value = mock_check

            access.verify()

        mock_logging = mock_deps['logging']
        mock_sleep = mock_deps['sleep']
        mock_probe.assert_has_calls(
            [call('https://test.website', config), call('https://test.website', config)],
            any_order=True,
        )
        mock_sleep.assert_called_once_with(3)
        mock_logging.assert_has_calls([
            call.info('Waiting for https://test.website to be available'),
            call.info('Last attempted access of target caused error: website broken'),
            call.warning('https://test.website could not be reached, attempting scan anyway'),
        ])

    def test_verify_raises_error_if_target_gives_500(self):
        config = Mock()
        config.availability_timeout = 1
        mock_check = Mock()
        mock_check.is_available.return_value = False
        mock_check.is_safe_to_scan.return_value = (True, None)
        mock_check.status_code.return_value = 502

        access = TargetAvailability('https://test.website', config)

        with self.assertRaises(TargetNotAccessibleError) as error:
            with patch('src.services.target_availability.TargetProbe') as mock_probe:
                mock_probe.return_value.send_ping.return_value = mock_check

                access.verify()

        self.assertIn('Target access check failed with HTTP error code: 502. Canceling scan', str(error.exception))

    def _now(self):
        return self._utcnows.pop()
