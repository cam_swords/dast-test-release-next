from unittest import TestCase
from unittest.mock import MagicMock

from src.zap_gateway.zap_database.message_results_parser import MessageResultsParser


class TestMessageResultsParser(TestCase):

    def setUp(self) -> None:
        self.http_headers_parser = MagicMock()
        self.http_status_parser = MagicMock()
        self.parser = MessageResultsParser(self.http_headers_parser, self.http_status_parser)

    def test_should_parse_messages(self):
        results = [('1',
                    'GET',
                    'http://host.docker.internal:8010/',
                    '\r\n'.join([
                        'GET http://host.docker.internal:8010/ HTTP/1.1',
                        'User-Agent: python-requests/2.20.1',
                    ]),
                    '\r\n'.join([
                        'HTTP/1.1 200 OK',
                        'Server: nginx/1.17.6',
                        '',
                    ]))]

        self.http_headers_parser.parse.return_value = 'http.headers'
        self.http_status_parser.parse.return_value = (200, 'OK')
        messages = self.parser.parse(results)

        self.assertEqual(1, len(messages))
        self.assertEqual('1', str(messages[0].message_id))
        self.assertEqual('GET', messages[0].request.method)
        self.assertEqual('http://host.docker.internal:8010/', messages[0].request.url)
        self.assertEqual('http.headers', messages[0].request.headers)
        self.assertEqual(200, messages[0].response.status)
        self.assertEqual('OK', messages[0].response.reason_phrase)
        self.assertEqual('http.headers', messages[0].response.headers)

    def test_should_not_return_messages_if_there_are_none(self):
        self.assertEqual(0, len(self.parser.parse([])))
        self.assertEqual(0, len(self.parser.parse([('GET',)])))
