from unittest import TestCase
from unittest.mock import MagicMock

from src.zap_gateway.server_configuration import ExcludeRulesConfigurationBuilder


class TestExcludeRulesConfigurationBuilder(TestCase):

    def test_should_exclude_rules(self):
        config = MagicMock(exclude_rules=['1001'])

        configuration = ExcludeRulesConfigurationBuilder(config).build()

        normalized = ' '.join(configuration)
        self.assertIn('-config globalalertfilter.filters.filter(0).ruleid=1001', normalized)
        self.assertIn('-config globalalertfilter.filters.filter(0).url=.*', normalized)
        self.assertIn('-config globalalertfilter.filters.filter(0).urlregex=true', normalized)
        self.assertIn('-config globalalertfilter.filters.filter(0).newrisk=-1', normalized)
        self.assertIn('-config globalalertfilter.filters.filter(0).enabled=true', normalized)

    def test_does_not_exclude_rules_when_none_given(self):
        config = MagicMock(exclude_rules=[])

        configuration = ExcludeRulesConfigurationBuilder(config).build()

        self.assertEqual([], configuration)
