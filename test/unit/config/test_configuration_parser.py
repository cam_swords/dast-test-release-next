from unittest import TestCase
from unittest.mock import MagicMock, patch

from src.config import InvalidConfigurationError
from src.config.configuration_parser import ConfigurationParser
from test.unit import utilities


def merge(a, b):
    result = a.copy()
    result.update(b)
    return result


class TestConfigurationParser(TestCase):
    DEFAULT_ENV = {'DAST_WEBSITE': 'http://website'}

    def setUp(self) -> None:
        self.parser = ConfigurationParser()

    def test_can_set_target_using_option(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertEqual(config.target, 'http://website')

    def test_can_set_target_using_environment(self):
        config = self.parser.parse([], {'DAST_WEBSITE': 'http://website'})
        self.assertEqual(config.target, 'http://website')

    def test_raises_error_when_target_not_a_valid_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['-t', 'not a URL'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('error: argument -t: not a URL is not a valid URL', output.stderr.getvalue())

    def test_can_set_spider_start_to_host_using_option(self):
        config = self.parser.parse(['-t', 'http://website', '--spider-start-at-host', 'False'], {})
        self.assertFalse(config.spider_start_at_host)

    def test_can_set_spider_start_to_host_using_environment(self):
        config = self.parser.parse(['-t', 'http://website'], {'DAST_SPIDER_START_AT_HOST': 'False'})
        self.assertFalse(config.spider_start_at_host)

    def test_can_set_spider_start_to_host_defaults_to_true(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertTrue(config.spider_start_at_host)

    def test_aggregate_vulnerabilities_defaults_to_false(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertFalse(config.aggregate_vulnerabilities)

    def test_can_set_aggregate_vulnerabilities_using_option(self):
        config = self.parser.parse(['-t', 'http://website', '--aggregate-vulnerabilities', 'True'], {})
        self.assertTrue(config.aggregate_vulnerabilities)

    def test_can_set_paths_to_scan_file_path_using_environment(self):
        with patch('src.config.configuration_parser.URLScanConfigurationParser.parse',
                   return_value=['http://website/1']):
            config = self.parser.parse(['-t', 'http://website'],
                                       {'DAST_PATHS_FILE': 'test/end-to-end/fixtures/url-scan/paths_to_scan.txt'})

        self.assertEqual(config.urls_to_scan, ['http://website/1'])

    def test_can_set_paths_to_scan_file_path_using_option(self):
        with patch('src.config.configuration_parser.URLScanConfigurationParser.parse',
                   return_value=['http://website/1']):
            config = self.parser.parse(['-t', 'http://website',
                                        '--paths-to-scan-file',
                                        'test/end-to-end/fixtures/url-scan/paths_to_scan.txt'], {})

        self.assertEqual(config.urls_to_scan, ['http://website/1'])

    def test_can_set_paths_to_scan_using_enviroment(self):
        config = self.parser.parse(['-t', 'http://website'],
                                   {'DAST_PATHS': '/,/1,/2?arg=1'})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2?arg=1'])

    def test_can_set_paths_to_scan_using_option(self):
        config = self.parser.parse(['-t', 'http://website',
                                    '--paths-to-scan', '/,/1,/2'], {})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2'])

    def test_urls_to_scan_are_correctly_built_when_target_ends_in_slash(self):
        config = self.parser.parse(['-t', 'http://website/',
                                    '--paths-to-scan', '/,/1,/2'], {})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2'])

    def test_urls_to_scan_are_correctly_built_when_path_does_not_start_with_slash(self):
        config = self.parser.parse(['-t', 'http://website',
                                    '--paths-to-scan', 'page1,page2'], {})
        self.assertEqual(config.urls_to_scan, ['http://website/page1', 'http://website/page2'])

    def test_urls_to_scan_are_an_empty_list_by_default(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertEqual(config.urls_to_scan, [])

    def test_urls_to_scan_error_when_url_is_invalid(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['-t', '//website',
                               '--paths-to-scan', '$[page1],page2'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('error: argument -t: //website is not a valid URL', output.stderr.getvalue())

    def test_white_space_is_removed_from_urls_to_scan(self):
        config = self.parser.parse(['-t', 'http://website',
                                    '--paths-to-scan', ' /,  /1,/2  '], {})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2'])

    def test_option_value_overrides_environment(self):
        config = self.parser.parse(['-t', 'http://website'], {'DAST_WEBSITE': 'http://another.website'})
        self.assertEqual(config.target, 'http://website')

    def test_can_set_api_specification_using_option(self):
        config = self.parser.parse(['--api-specification', 'http://website/api.spec'], {})
        self.assertEqual(config.api_specification, 'http://website/api.spec')
        self.assertTrue(config.is_api_scan)

    def test_can_set_api_specification_using_environment_alias(self):
        config = self.parser.parse([], {'DAST_API_SPECIFICATION': 'http://website/api.spec'})
        self.assertEqual(config.api_specification, 'http://website/api.spec')
        self.assertTrue(config.is_api_scan)

    def test_is_not_api_scan_when_api_format_not_specified(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertFalse(config.is_api_scan)

    def test_domain_override_can_be_set_using_option(self):
        config = self.parser.parse(['-O', 'my.host.com', '--api-specification', 'http://api.spec'], {})
        self.assertEqual(config.zap_api_host_override, 'my.host.com')

    def test_domain_override_can_be_set_using_environment(self):
        config = self.parser.parse([], merge({'DAST_API_HOST_OVERRIDE': 'my.host.com'}, self.DEFAULT_ENV))
        self.assertEqual(config.zap_api_host_override, 'my.host.com')

    def test_can_not_set_invalid_url_as_auth_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--auth-url', 'loremipsum'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-url: loremipsum is not a valid URL', output.stderr.getvalue())

    def test_can_set_auth_url_using_option(self):
        config = self.parser.parse(['--auth-url', 'http://website'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_set_auth_url_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_URL': 'http://website'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_set_auth_url_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_URL': 'http://website'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_not_set_invalid_url_as_auth_verification_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--auth-verification-url', 'loremipsum'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-verification-url: loremipsum is not a valid URL', output.stderr.getvalue())

    def test_can_set_auth_verification_url_using_option(self):
        config = self.parser.parse(['--auth-verification-url', 'http://website'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_verification_url, 'http://website')

    def test_can_set_auth_verification_url_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_VERIFICATION_URL': 'http://website'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_verification_url, 'http://website')

    def test_can_set_auth_username_using_option(self):
        config = self.parser.parse(['--auth-username', 'username'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_username_using_environment(self):
        config = self.parser.parse([], merge({'DAST_USERNAME': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_username_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_USERNAME': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_password_using_option(self):
        config = self.parser.parse(['--auth-password', 'password'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_auth_password_using_environment(self):
        config = self.parser.parse([], merge({'DAST_PASSWORD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_auth_password_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_PASSWORD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_username_field_using_option(self):
        config = self.parser.parse(['--auth-username-field', 'username'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_username_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_USERNAME_FIELD': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_username_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_USERNAME_FIELD': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_password_field_using_option(self):
        config = self.parser.parse(['--auth-password-field', 'password'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_password_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_PASSWORD_FIELD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_password_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_PASSWORD_FIELD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_auth_submit_field_using_option(self):
        config = self.parser.parse(['--auth-submit-field', 'field'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_submit_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_submit_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_option(self):
        config = self.parser.parse(['--auth-first-submit-field', 'field'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_FIRST_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_FIRST_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_display_using_option(self):
        config = self.parser.parse(['--auth-display', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_display_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_DISPLAY': 'True'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_display_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_DISPLAY': 'True'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_auto_using_option(self):
        config = self.parser.parse(['--auth-auto', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_auto, True)

    def test_can_set_auth_auto_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_AUTO': 'true'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_auto, True)

    def test_can_set_auth_auto_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_AUTO': 'true'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_auto, True)

    def test_exclude_urls_is_empty_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_urls, [])

    def test_can_not_set_exclude_urls_to_have_any_invalid_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--auth-exclude-urls', 'http://website1 , http://website2   , invalidurl'],
                              self.DEFAULT_ENV)

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-exclude-urls: invalidurl is not a valid URL', output.stderr.getvalue())

    def test_can_set_exclude_urls_using_option(self):
        config = self.parser.parse(['--auth-exclude-urls', 'http://website1 , http://website2  '], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_can_set_exclude_urls_using_legacy_environment(self):
        environment = merge({'DAST_AUTH_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_can_set_exclude_urls_using_second_legacy_environment(self):
        environment = merge({'AUTH_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_can_set_exclude_urls_using_environment(self):
        environment = merge({'DAST_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_enable_silent_when_auto_update_addons_is_false(self):
        config = self.parser.parse(['--auto-update-addons', 'False'], self.DEFAULT_ENV)
        self.assertEqual(config.silent, True)

    def test_enable_silent_when_auto_update_addons_is_true_and_silent_is_true(self):
        config = self.parser.parse(['--auto-update-addons', 'True', '-z-silent'], self.DEFAULT_ENV)
        self.assertEqual(config.silent, True)

    def test_silent_disabled_when_auto_update_addons_is_true_and_silent_is_not_set(self):
        config = self.parser.parse(['--auto-update-addons', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.silent, False)

    def test_enable_auto_update_addons_using_option(self):
        config = self.parser.parse(['--auto-update-addons', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, True)

    def test_can_set_exclude_rules_using_option(self):
        config = self.parser.parse(['--exclude-rules', ' 10001 , 20002  '], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_rules, ['10001', '20002'] + ConfigurationParser.GITLAB_EXCLUDE_RULES)

    def test_can_set_exclude_rules_using_environment(self):
        environment = merge({'DAST_EXCLUDE_RULES': '56,57'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_rules, ['56', '57'] + ConfigurationParser.GITLAB_EXCLUDE_RULES)

    def test_only_gitlab_exclude_rules_are_excluded_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_rules, ConfigurationParser.GITLAB_EXCLUDE_RULES)

    def test_can_set_request_headers_using_option(self):
        config = self.parser.parse(['--request-headers', ' Authorization: Bearer 123456789 , Accept: */*  '],
                                   self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {'Authorization': 'Bearer 123456789', 'Accept': '*/*'})

    def test_can_set_request_headers_using_environment(self):
        environment = merge({'DAST_REQUEST_HEADERS': 'Cache-control: no-cache'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertDictEqual(config.request_headers, {'Cache-control': 'no-cache'})

    def test_setting_request_headers_aborts_when_format_invalid(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--request-headers', 'Too:many:colons'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('Failed to parse request headers, aborting', output.stderr.getvalue())

    def test_no_request_headers_are_set_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {})

    def test_disable_auto_update_addons_using_option(self):
        config = self.parser.parse(['--auto-update-addons', 'False'], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, False)

    def test_auto_update_addons_is_false_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, False)

    def test_disable_auto_update_addons_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTO_UPDATE_ADDONS': '0'}, self.DEFAULT_ENV))
        self.assertEqual(config.auto_update_addons, False)

    def test_can_set_full_scan_using_option_passing_true(self):
        config = self.parser.parse(['--full-scan', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_using_option_passing_1(self):
        config = self.parser.parse(['--full-scan', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_using_option_passing_false(self):
        config = self.parser.parse(['--full-scan', 'false'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_option_passing_0(self):
        config = self.parser.parse(['--full-scan', '0'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_option(self):
        config = self.parser.parse(['--full-scan', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_should_by_falsy_when_junk_is_passed_in(self):
        config = self.parser.parse(['--full-scan', 'not-a-boolean-value'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_environment(self):
        environment = merge({'DAST_FULL_SCAN_ENABLED': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.full_scan, True)

    def test_can_set_write_addons_to_update_file_using_option_passing_true(self):
        config = self.parser.parse(['--write-addons-to-update-file', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.write_addons_to_update_file, True)

    def test_can_set_write_addons_to_update_file_using_option_passing_false(self):
        config = self.parser.parse(['--write-addons-to-update-file', 'false'], self.DEFAULT_ENV)
        self.assertEqual(config.write_addons_to_update_file, False)

    def test_can_set_write_addons_to_update_file_using_option(self):
        config = self.parser.parse(['--write-addons-to-update-file', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.write_addons_to_update_file, True)

    def test_can_set_validate_domain_using_option_passing_true(self):
        config = self.parser.parse(['--validate-domain', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_validate_domain_using_option_passing_1(self):
        config = self.parser.parse(['--validate-domain', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_validate_domain_using_option_passing_false(self):
        config = self.parser.parse(['--validate-domain', 'false'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, False)

    def test_can_set_validate_domain_using_option_passing_0(self):
        config = self.parser.parse(['--validate-domain', '0'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, False)

    def test_can_set_domain_validation_using_option(self):
        config = self.parser.parse(['--validate-domain', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_full_scan_domain_validation_required_using_environment(self):
        environment = merge({'DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_availability_timeout_using_option(self):
        config = self.parser.parse(['--availability-timeout', '10'], self.DEFAULT_ENV)
        self.assertEqual(config.availability_timeout, 10)

    def test_can_set_availability_timeout_using_environment(self):
        environment = merge({'DAST_TARGET_AVAILABILITY_TIMEOUT': '20'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.availability_timeout, 20)

    def test_default_availability_timeout(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.availability_timeout, 60)

    def test_can_set_skip_target_check_using_option(self):
        config = self.parser.parse(['--skip-target-check', 'true'], self.DEFAULT_ENV)
        self.assertTrue(config.skip_target_check)

    def test_can_set_skip_target_check_using_environment(self):
        environment = merge({'DAST_SKIP_TARGET_CHECK': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertTrue(config.skip_target_check)

    def test_default_skip_target_check_is_false(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertFalse(config.skip_target_check)

    def test_can_set_zap_option_mins(self):
        config = self.parser.parse(['-m', '5'], self.DEFAULT_ENV)
        self.assertEqual(config.spider_mins, 5)

    def test_can_set_zap_option_mins_using_environment_variable(self):
        environment = merge({'DAST_SPIDER_MINS': '0'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.spider_mins, 0)

    def test_default_spider_mins_when_passive_scan_and_not_set(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.spider_mins, 1)

    def test_unlimited_default_spider_mins_when_full_scan_and_not_set(self):
        config = self.parser.parse(['--full-scan', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.spider_mins, 0)

    def test_can_set_zap_option_report_html(self):
        config = self.parser.parse(['-r', 'report_html'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_html, 'report_html')

    def test_can_set_zap_long_option_report_html(self):
        config = self.parser.parse(['--html-report', 'report_html'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_html, 'report_html')

    def test_can_set_zap_option_report_html_using_environment(self):
        environment = merge({'DAST_HTML_REPORT': 'report.html'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_report_html, 'report.html')

    def test_can_set_zap_option_report_md(self):
        config = self.parser.parse(['-w', 'report_md'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_md, 'report_md')

    def test_can_set_zap_long_option_report_md(self):
        config = self.parser.parse(['--markdown-report', 'report_md'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_md, 'report_md')

    def test_can_set_zap_option_report_md_using_environment(self):
        environment = merge({'DAST_MARKDOWN_REPORT': 'report.md'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_report_md, 'report.md')

    def test_can_set_zap_option_report_xml(self):
        config = self.parser.parse(['-x', 'report_xml'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_xml, 'report_xml')

    def test_can_set_zap_long_option_report_xml(self):
        config = self.parser.parse(['--xml-report', 'report_xml'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_xml, 'report_xml')

    def test_can_set_zap_option_report_xml_using_environment(self):
        environment = merge({'DAST_XML_REPORT': 'report.xml'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_report_xml, 'report.xml')

    def test_defaults_to_not_a_browserker_scan(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_scan, False)

    def test_can_trigger_browserker_scan(self):
        config = self.parser.parse(['--browserker-scan'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_scan, True)

    def test_can_trigger_browserker_scan_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_SCAN': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_scan, True)

    def test_raises_error_if_ajax_spider_scan_and_browserker_scan_configured(self):
        with self.assertRaises(InvalidConfigurationError) as error:
            options = {'DAST_BROWSERKER_SCAN': 'true', 'DAST_USE_AJAX_SPIDER': 'true'}
            self.parser.parse([], merge(options, self.DEFAULT_ENV))

        self.assertIn('Browserker cannot be used with an AJAX spider scan', str(error.exception))

    def test_raises_error_if_api_scan_and_browserker_scan_configured(self):
        with self.assertRaises(InvalidConfigurationError) as error:
            options = {'DAST_BROWSERKER_SCAN': 'true', 'DAST_API_SPECIFICATION': 'api_spec.yml'}
            self.parser.parse([], merge(options, self.DEFAULT_ENV))

        self.assertIn('Browserker cannot be used with an API scan', str(error.exception))

    def test_raises_error_if_paths_to_scan_and_browserker_scan_configured(self):
        with self.assertRaises(InvalidConfigurationError) as error:
            options = {'DAST_BROWSERKER_SCAN': 'true', 'DAST_PATHS': 'true'}
            self.parser.parse([], merge(options, self.DEFAULT_ENV))

        self.assertIn('Browserker cannot be used when a list of paths to scan is provided', str(error.exception))

    def test_raises_error_if_file_with_paths_to_scan_and_browserker_scan_configured(self):
        parse_call = 'src.config.configuration_parser.URLScanConfigurationParser.parse'

        with patch(parse_call, return_value=['http://site']), self.assertRaises(InvalidConfigurationError) as error:
            options = {'DAST_BROWSERKER_SCAN': 'true', 'DAST_PATHS_FILE': 'paths.txt'}
            self.parser.parse([], merge(options, self.DEFAULT_ENV))

        self.assertIn('Browserker cannot be used when a file of paths to scan is provided', str(error.exception))

    def test_sets_default_max_actions(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_actions, 10000)

    def test_can_set_browserker_max_actions_using_cli(self):
        config = self.parser.parse(['--browserker-max-actions', '456'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_actions, 456)

    def test_can_set_browserker_max_actions_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_MAX_ACTIONS': '224'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_max_actions, 224)

    def test_sets_default_max_depth(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_depth, 10)

    def test_can_set_browserker_max_depth_using_cli(self):
        config = self.parser.parse(['--browserker-max-depth', '7'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_depth, 7)

    def test_can_set_browserker_max_depth_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_MAX_DEPTH': '15'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_max_depth, 15)

    def test_sets_default_number_of_browsers(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_number_of_browsers, 3)

    def test_can_set_browserker_number_of_browsers_using_cli(self):
        config = self.parser.parse(['--browserker-number-of-browsers', '2'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_number_of_browsers, 2)

    def test_can_set_browserker_number_of_browsers_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_NUMBER_OF_BROWSERS': '1'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_number_of_browsers, 1)

    def test_sets_default_excluded_hosts(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_excluded_hosts, [])

    def test_can_set_browserker_excluded_hosts_using_cli(self):
        config = self.parser.parse(['--browserker-excluded-hosts', '  domain-a.com , domain-b.com'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_excluded_hosts, ['domain-a.com', 'domain-b.com'])

    def test_can_set_browserker_excluded_hosts_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_EXCLUDED_HOSTS': 'site.com'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_excluded_hosts, ['site.com'])

    def test_sets_default_ignored_hosts(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_ignored_hosts, [])

    def test_can_set_browserker_ignored_hosts_using_cli(self):
        config = self.parser.parse(['--browserker-ignored-hosts', '  domain-a.com , domain-b.com'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_ignored_hosts, ['domain-a.com', 'domain-b.com'])

    def test_can_set_browserker_ignored_hosts_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_IGNORED_HOSTS': 'site.com'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_ignored_hosts, ['site.com'])

    def test_sets_default_allowed_hosts_to_target(self):
        environment = merge(self.DEFAULT_ENV, {'DAST_WEBSITE': 'http://site.com'})
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_allowed_hosts, ['site.com'])

    def test_can_set_browserker_allowed_hosts_using_cli(self):
        config = self.parser.parse(
            ['-t', 'http://site.com', '--browserker-allowed-hosts', '  domain-a.com , domain-b.com'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_allowed_hosts, ['domain-a.com', 'domain-b.com', 'site.com'])

    def test_can_set_browserker_allowed_hosts_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_ALLOWED_HOSTS': 'site.com'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_allowed_hosts, ['site.com', 'website'])

    def test_sets_default_max_attack_failures(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_attack_failures, 5)

    def test_can_set_browserker_max_attack_failures_using_cli(self):
        config = self.parser.parse(['--browserker-max-attack-failures', '2'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_attack_failures, 2)

    def test_can_set_browserker_max_attack_failures_using_environment_variables(self):
        environment = merge({'DAST_BROWSERKER_MAX_ATTACK_FAILURES': '1'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_max_attack_failures, 1)

    def test_can_set_browserker_cookies_using_option(self):
        config = self.parser.parse(['--browserker-cookies', 'abtesting_group:3,region:locked'],
                                   self.DEFAULT_ENV)
        self.assertDictEqual(config.browserker_cookies, {'abtesting_group': '3', 'region': 'locked'})

    def test_can_set_browserker_cookies_using_environment(self):
        environment = merge({'DAST_BROWSERKER_COOKIES': 'name:Archer'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertDictEqual(config.browserker_cookies, {'name': 'Archer'})

    def test_setting_browserker_cookies_aborts_when_format_invalid(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--browserker-cookies', 'Too:many:colons'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('Failed to parse cookies, aborting', output.stderr.getvalue())

    def test_no_browserker_cookies_are_set_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertDictEqual(config.browserker_cookies, {})

    def test_can_set_zap_option_report_include_alpha_rules(self):
        config = self.parser.parse(['-a'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_include_alpha, True)

    def test_can_set_zap_option_report_include_alpha_vulns_using_environment(self):
        environment = merge({'DAST_INCLUDE_ALPHA_VULNERABILITIES': 'false'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_include_alpha, False)

    def test_can_set_zap_option_report_show_debug_msgs(self):
        config = self.parser.parse(['-d'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_debug, True)

    def test_can_set_zap_option_report_show_debug_msgs_using_environment(self):
        environment = merge({'DAST_DEBUG': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_debug, True)

    def test_can_set_zap_option_port(self):
        config = self.parser.parse(['-P', '2001'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_port, 2001)

    def test_zap_port_defaults_to_a_free_port_if_not_set(self):
        with patch('src.config.configuration_parser.System') as System:
            system = MagicMock()
            system.get_free_port.return_value = 43001
            System.return_value = system
            config = self.parser.parse([], self.DEFAULT_ENV)
            self.assertEqual(config.zap_port, 43001)

    def test_can_set_zap_option_delay_in_seconds(self):
        config = self.parser.parse(['-D', '12'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_delay_in_seconds, '12')

    def test_can_set_zap_option_default_info(self):
        config = self.parser.parse(['-i'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_default_info, True)

    def test_can_set_zap_option_no_fail_on_warn(self):
        config = self.parser.parse(['-I'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_no_fail_on_warn, True)

    def test_can_set_zap_ajax_spider_using_zap_env_variable(self):
        environment = merge({'DAST_ZAP_USE_AJAX_SPIDER': True}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_use_ajax_spider, True)

    def test_can_set_zap_ajax_spider_using_env_variable(self):
        environment = merge({'DAST_USE_AJAX_SPIDER': True}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_use_ajax_spider, True)

    def test_can_set_zap_option_use_ajax_spider(self):
        config = self.parser.parse(['-j'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_use_ajax_spider, True)

    def test_can_set_zap_option_min_level(self):
        config = self.parser.parse(['-l', 'INFO'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_min_level, 'INFO')

    def test_can_set_zap_option_context(self):
        config = self.parser.parse(['-n', 'context.file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_context_file, 'context.file')

    def test_can_set_zap_option_progress(self):
        config = self.parser.parse(['-p', 'progress.file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_progress_file, 'progress.file')

    def test_can_set_zap_option_short_format(self):
        config = self.parser.parse(['-s'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_short_format, True)

    def test_can_set_zap_connection_attempts_using_option(self):
        config = self.parser.parse(['--zap-max-connection-attempts', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 1)

    def test_can_set_zap_connection_attempts_using_environment(self):
        environment = merge({'DAST_ZAP_MAX_CONNECTION_ATTEMPTS': '34'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_max_connection_attempts, 34)

    def test_connection_attempts_has_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 600)

    def test_zap_connection_attempts_does_not_allow_negative_integers(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--zap-max-connection-attempts', '-1'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('cannot be negative', output.stderr.getvalue())

    def test_ignores_deprecated_option_if_other_timeouts_have_been_set(self):
        config = self.parser.parse(['-T', '1', '--zap-max-connection-attempts', '2'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 2)
        self.assertEqual(config.passive_scan_max_wait_time, 600)

    def test_can_set_connection_attempts_and_passive_scan_wait_time_using_option(self):
        config = self.parser.parse(['-T', '5', '--zap-connect-sleep-seconds', '5'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_connect_sleep_seconds, 5)
        self.assertEqual(config.zap_max_connection_attempts, 60)
        self.assertEqual(config.passive_scan_max_wait_time, 5)

    def test_can_set_connection_attempts_and_passive_scan_wait_time_when_no_sleep_seconds(self):
        config = self.parser.parse(['-T', '5', '--zap-connect-sleep-seconds', '0'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 300)
        self.assertEqual(config.passive_scan_max_wait_time, 5)

    def test_can_set_zap_sleep_seconds_using_option(self):
        config = self.parser.parse(['--zap-connect-sleep-seconds', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_connect_sleep_seconds, 1)

    def test_can_set_zap_sleep_seconds_using_environment(self):
        environment = merge({'DAST_ZAP_CONNECT_SLEEP_SECONDS': '60'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_connect_sleep_seconds, 60)

    def test_sleep_seconds_has_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.zap_connect_sleep_seconds, 1)

    def test_can_set_passive_scan_wait_time_using_option(self):
        config = self.parser.parse(['--passive-scan-max-wait-time', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.passive_scan_max_wait_time, 1)

    def test_can_set_passive_scan_wait_time_using_environment(self):
        environment = merge({'DAST_PASSIVE_SCAN_MAX_WAIT_TIME': '34'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.passive_scan_max_wait_time, 34)

    def test_passive_scan_wait_time_has_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.passive_scan_max_wait_time, 600)

    def test_can_set_zap_option_other_options(self):
        config = self.parser.parse(['-z', 'funtimes=3'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_other_options, 'funtimes=3')

    def test_can_set_zap_option_other_options_using_environment(self):
        environment = merge({'DAST_ZAP_CLI_OPTIONS': 'badtimes=0'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_other_options, 'badtimes=0')

    def test_can_set_zap_option_log_configuration(self):
        config = self.parser.parse(['--zap-log-configuration', 'opts'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_log_configuration, 'opts')

    def test_can_set_zap_option_log_configuration_using_environment(self):
        environment = merge({'DAST_ZAP_LOG_CONFIGURATION': 'opts'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_log_configuration, 'opts')

    def test_can_set_http_headers_to_mask_using_option(self):
        config = self.parser.parse(['--mask-http-headers', ' Authorization   , X-Vault-Token'], self.DEFAULT_ENV)
        self.assertEqual(config.http_headers_to_mask, ['Authorization', 'X-Vault-Token'])

    def test_can_set_http_headers_to_mask_using_environment(self):
        environment = merge({'DAST_MASK_HTTP_HEADERS': 'Authorization'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.http_headers_to_mask, ['Authorization'])

    def test_can_set_no_http_headers_to_mask_when_environment_variable_is_empty(self):
        environment = merge({'DAST_MASK_HTTP_HEADERS': ''}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.http_headers_to_mask, [])

    def test_uses_default_values_for_http_headers_to_mask_when_not_set(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertIn('Authorization', config.http_headers_to_mask)

    def test_can_set_script_directories_using_option(self):
        config = self.parser.parse(['--script-dirs', '/home/zap/dast-scripts , /build/my-scripts  '], self.DEFAULT_ENV)
        self.assertIn('/home/zap/dast-scripts', config.script_dirs)
        self.assertIn('/build/my-scripts', config.script_dirs)

    def test_can_set_script_directories_using_environment_variable(self):
        environment = merge({'DAST_SCRIPT_DIRS': '/home/zap/scripts,/home/zap/more-scripts'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertIn('/home/zap/scripts', config.script_dirs)
        self.assertIn('/home/zap/more-scripts', config.script_dirs)

    def test_script_dirs_contains_dast_api_scripts(self):
        environment = merge({'DAST_SCRIPT_DIRS': '/home/zap/scripts',
                             'DAST_API_SPECIFICATION': 'http://website/api.spec'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertIn(ConfigurationParser.DAST_API_SCAN_SCRIPTS_DIR, config.script_dirs)

    def test_script_dirs_contains_dast_normal_scan_scripts(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertIn(ConfigurationParser.DAST_NORMAL_SCAN_SCRIPTS_DIR, config.script_dirs)

    def test_raises_error_when_DAST_PATHS_is_specificed_but_DAST_WEBSITE_is_not(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse([], {'DAST_PATHS': '/1'})

        self.assertIn('When DAST_PATHS is defined DAST_WEBSITE must also be set.', str(error.exception))

    def test_raises_error_when_DAST_PATHS_FILE_is_specificed_but_DAST_WEBSITE_is_not(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse([], {'DAST_PATHS_FILE': 'test/end-to-end/fixtures/url-scan/paths_to_scan.txt'})

        self.assertIn('When DAST_PATHS_FILE is defined DAST_WEBSITE must also be set.', str(error.exception))

    def test_raises_error_when_DAST_PATHS_and_DAST_PATHS_FILE_are_both_specified(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse(['-t', 'http://website'], {'DAST_PATHS': '/1', 'DAST_PATHS_FILE': '/path/to/file.txt'})

        self.assertIn('DAST_PATHS and DAST_PATHS_FILE can not be defined at the same time.', str(error.exception))

    def test_raises_error_when_no_target_or_api_specification(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse([], {})

        self.assertIn('Either DAST_WEBSITE or DAST_API_SPECIFICATION must be set', str(error.exception))

    def test_raises_error_if_ajax_spider_and_api_scan_both_configured(self):
        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], {'DAST_USE_AJAX_SPIDER': True, 'DAST_API_SPECIFICATION': 'api_spec.yml'})

        self.assertIn(
            'The AJAX Spider (configured with DAST_USE_AJAX_SPIDER, DAST_ZAP_USE_AJAX_SPIDER, --use-ajax-spider, '
            'or -j) cannot be used with an API scan (configured with DAST_API_SPECIFICATION or '
            '--api-specification)',
            str(error.exception),
        )

    def test_is_next_major_release_if_future_file_is_present(self):
        with patch('src.config.configuration_parser.path') as path:
            path.isfile.return_value = True

            config = self.parser.parse([], self.DEFAULT_ENV)
            self.assertTrue(config.next_major_release)

    def test_is_not_next_major_release_if_future_file_is_not_present(self):
        with patch('src.config.configuration_parser.path') as path:
            path.isfile.return_value = False

            config = self.parser.parse([], self.DEFAULT_ENV)
            self.assertFalse(config.next_major_release)
