from invoke import Collection, task

from lib.tests import Tests


@task
def unit(_, file='', test=''):
    """Run unit tests (options: --file, --test)."""
    Tests('Unit', 'test/unit', file, test).execute()


@task
def integration(_, file='', test=''):
    """Run integration tests (options: --file, --test)."""
    Tests('Integration', 'test/integration', file, test).execute()


test = Collection(unit, integration)
