from os import getenv, getuid, path
from pwd import getpwuid

from invoke import Collection, run, task

from lib.server import DVWAServer
from src.services.addon_json_parser import AddonJSONParser


@task
def build(context, set_zap_and_browserker_uid=False):
    """Build a Docker image with tag dast:latest."""
    if set_zap_and_browserker_uid:
        zap_uid = getenv('ZAP_UID', default=getpwuid(getuid()).pw_uid)
        browserker_uid = getenv('BROWSERKER_UID', default=1000)

        run(f'docker build --build-arg ZAP_UID={zap_uid} --build-arg BROWSERKER_UID={browserker_uid} . -t dast')
    else:
        run('docker build . -t dast')


@task
def shell(context):
    """Start a Docker dast:latest container and run a shell."""
    dast_dir = path.join(path.dirname(path.realpath(__file__)), '..', '..')
    run(f'docker run -ti -v {dast_dir}:/output -w /zap dast /bin/bash ', pty=True)


@task
def analyze_dvwa(context):
    """Run dast:latest against the running DVWA instance."""
    dvwa_server = DVWAServer(run, port=8050)
    dast_dir = path.join(path.dirname(path.realpath(__file__)), '..', '..')
    environment = ' '.join([f'--env {variable}'
                            for variable in (dvwa_server.user_login_configuration())])
    target = dvwa_server.target()

    run(f'docker run -ti --rm {environment} -v {dast_dir}:/output dast /analyze -t {target}', pty=True)


@task
def parse_addon_json(context):
    """Parse the contents of the addons.json"""
    print(AddonJSONParser('addons.json').parse())


analyze = Collection('analyze', dvwa=analyze_dvwa)
dast = Collection(analyze, build, shell, parse_addon_json)
