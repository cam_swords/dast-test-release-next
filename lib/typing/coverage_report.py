import re


class CoverageReport:
    MATCH_PERCENT = r'\d+\.\d+'
    METRICS_REPORT_NAME = 'artifacts/metrics.txt'

    def generate_metrics(self):
        coverage_percent = None

        with open('artifacts/mypy.txt', 'r') as mypy_report, \
             open(self.METRICS_REPORT_NAME, 'w') as metrics_report:
            coverage_summary = mypy_report.readlines()[-2]
            typing_imprecision = float(re.search(self.MATCH_PERCENT, coverage_summary).group(0))
            coverage_percent = 100 - typing_imprecision

            metrics_report.write(f'typing_coverage_percent: {coverage_percent}\n')

        print(f'Generated typing coverage OpenMetrics report: {self.METRICS_REPORT_NAME}')
        print(f'DAST src type coverage: {coverage_percent}%')
