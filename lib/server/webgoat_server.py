from .host import Host


class WebGoatServer:

    IMAGE_SHA = 'bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e'
    IMAGE = f'registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:{IMAGE_SHA}'

    def __init__(self, run, port=80):
        self.run = run
        self.port = port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name webgoat_server '
                 f'-p {self.port}:8080 '
                 '-v "${PWD}/test/end-to-end/fixtures/webgoat-8.0.0.M21":/home/webgoat/.webgoat-8.0.0.M21 '
                 f'-d {WebGoatServer.IMAGE}')

        print(f'WebGoat server started at http://{self.host.name()}:{self.port}/WebGoat')
        print('Username: someone, Password: P@ssw0rd')
        return self

    def stop(self):
        self.run('docker rm --force webgoat_server >/dev/null 2>&1 || true')
        return self
